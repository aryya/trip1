<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>叩丁狼部门</title>
    <#include "../common/header.ftl">
    <script type="text/javascript" src="/treeview/bootstrap-treeview.min.js"></script>
    <link href="/treeview/bootstrap-treeview.min.css" type="text/css" rel="stylesheet">

    <script>
        $(function () {
            $.get("/region/listByParentId.do", {type: "tree"}, function (data) {
                $('#treeview').treeview({
                    data: [{text: '全部地区', nodes: data}],
                    showTags: true,
                    lazyLoad: function (node) {
                        //查询该节点下的子节点
                        $.get("/region/listByParentId.do", {parentId: node.id, type: "tree"}, function (data) {
                            //调用插件的方法添加节点(第一个参数是要添加的节点,第二个参数是要添加到哪里--父节点)
                            $('#treeview').treeview('addNode', [data, node]);
                        })
                    },
                    onNodeSelected: function (event, data) {//选中节点的事件
                        $.get("/region/listByParentId.do", {parentId: data.id}, function (data) {
                            //循环之前清空内容
                            $("#regionTb tbody").empty();
                            $.each(data, function (index, ele) {
                                var tr = $("#template tr").clone(true);//克隆事件
                                //设置内容
                                $(tr).find("td:nth-child(1)").html(index + 1);
                                $(tr).find("td:nth-child(2)").html(ele.name);
                                $(tr).find("a").attr("data-json", ele.json);

                                if(ele.state==1){
                                    $(tr).find("a:last").html("取消推荐")
                                }

                                $("#regionTb tbody").append(tr);
                            })
                        })
                    }
                })
            });

            //编辑按钮,做数据回显
            $(".btn-input").click(function () {
                var node = $(this).data("json");
                $("#editForm")[0].reset();
                $("[name='id']").val(node.id);
                $("[name='name']").val(node.name);
                $("[name='parent.id']").val(node.parentId);
                $("[name='parent.name']").val(node.parentName);
                $("#regionModal").modal("show");
            });


            //新增按钮
            $(".btn-add").click(function () {
                //获取tree中选择的地区
                var node = $('#treeview').treeview('getSelected');
                $("#editForm")[0].reset();
                $("[name='parent.id']").val(node[0].id);
                $("[name='parent.name']").val(node[0].text);
                $("#regionModal").modal("show");

            });

            /*提交按钮,表单提交*/
            $('#saveBtn').click(function () {
                /*提交表单用ajax异步提交*/
                $('#editForm').ajaxSubmit(function (data) {
                    if (data.success) {
                        $.messager.alert("温馨提示:", "保存成功,2s后跳转");
                        setTimeout(function () {
                            window.location.reload();
                        }, 2000);
                    }
                });
            });


            //点击推荐按钮
            $(".btn-commend").click(function(){
                var node=$(this).data("json");
                var state = 1;
                if(node.state==1){
                    state=0;
                }
                $.get('/region/changeState.do',{id:node.id,state:state},function () {
                    $.messager.confirm("提示","修改成功",function(){
                        window.location.reload();
                    });
                })
            });

        });

    </script>
</head>
<body>
<table id="template" style="display: none">
    <tr>
        <td></td>
        <td></td>
        <td>
            <a class="btn btn-info btn-xs btn-input">
                <span class="glyphicon glyphicon-pencil"></span>编辑
            </a>
            <a class="btn btn-info btn-xs btn-commend">
                <span class="glyphicon glyphicon-star"></span>设为推荐
            </a>
        </td>
    </tr>

</table>

<div class="container " style="margin-top: 20px">
    <#include "../common/top.ftl">
    <div class="row">
        <div class="col-sm-3">
            <#assign currentMenu="region"/>
            <#include "../common/menu.ftl">
        </div>
        <div class="col-sm-9">
            <div class="row">
                <div class="col-sm-12">
                    <h1 class="page-head-line">地区管理</h1>
                </div>
            </div>
            <div class="row">
                <div class="form-group" style="margin-left: 20px">
                    <a href="javascript:void(-1);" class="btn btn-success btn-add" id="addRegionBtn">添加地区</a>
                </div>
            </div>
            <!--高级查询--->
            <form class="form-inline" id="searchForm" action="/region/list.do" method="post">
                <input type="hidden" name="currentPage" id="currentPage" value="1">
            </form>

            <div class="row">
                <div class="col-sm-4">
                    <div id="treeview"></div>
                </div>

                <div class="col-sm-8">
                    <table class="table table-striped table-hover" id="regionTb">
                        <thead>
                        <tr>
                            <th>编号</th>
                            <th>名称</th>

                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<#--编辑模态框-->
<div id="regionModal" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">编辑/增加</h4>
            </div>
            <div class="modal-body">
                <form id="editForm" class="form-horizontal" method="post" action="/region/saveOrUpdate.do">
                    <input id="regionId" type="hidden" name="id" value=""/>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">名称</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="name" placeholder="地区/景区名称">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">上级地区</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="parent.name" readonly>
                            <input type="hidden" class="form-control" name="parent.id">
                        </div>
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <a href="javascript:void(0);" class="btn btn-success" id="saveBtn" aria-hidden="true">保存</a>
                <a href="javascript:void(0);" class="btn" data-dismiss="modal" aria-hidden="true">关闭</a>
            </div>
        </div>
    </div>
</div>

</body>
</html>