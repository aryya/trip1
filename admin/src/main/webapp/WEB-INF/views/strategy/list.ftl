<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>叩丁狼部门</title>
    <#include "../common/header.ftl">

    <script>
        $(function () {

            //点击修改, 显示数据
            $('.reviseBtn').click(function () {
                var json = $(this).data("json");
                $('[name="id"]').val(json.id);
                $('#editForm [name=title]').val(json.title);
                $('#editForm [name=subTitle]').val(json.subTitle);
                $('#coverUrl').attr("src", json.coverUrl);
                $('input[name=coverUrl]').val(json.coverUrl);
                $("select[name='place.id']").val(json.placeId);
                $('select[name=state]').val(json.state);
                $('#travelCommendModal').modal("show")
            });

            //新增数据   $("#editForm")[0].reset();
            $('#addStrategyBtn').click(function () {
                $('#editForm')[0].reset();
                $('#travelCommendModal').modal("show")
            });

            //提交表单
            $('#saveBtn').click(function () {
                $('#editForm').ajaxSubmit(function (data) {
                    successAlert(data);
                })
            });

            //绑定点击按钮和文件框
            $('#uploadImgBtn').click(function () {
                $('#file').click();
            });

            //文件框绑定改变事件
            $('#file').change(function () {
                if ($(this).val()) {
                    $('#imageForm').ajaxSubmit(function (data) {
                        $('#coverUrl').attr("src", data.url);
                        $('.modal-body [name=coverUrl]').val(data.url);

                    })
                }
            })
        })

    </script>

</head>
<body>

<div class="container " style="margin-top: 20px">
    <#include "../common/top.ftl">
    <div class="row">
        <div class="col-sm-3">
            <#assign currentMenu="strategy"/>
            <#include "../common/menu.ftl">
        </div>
        <div class="col-sm-9">
            <div class="row">
                <div class="col-sm-12">
                    <h1 class="page-head-line">攻略管理</h1>
                </div>
            </div>
            <!--高级查询--->
            <form class="form-inline" id="searchForm" action="/strategy/list.do" method="post">
                <input type="hidden" name="currentPage" id="currentPage" value="1">
                <div class="form-group">
                    <label for="keyword">关键字:</label>
                    <input type="text" class="form-control" id="keyword" name="keyword" value="${(qo.keyword)!}"
                           placeholder="请输入标题">
                </div>
                <div class="form-group">
                    <button id="query" type="submit" class="btn btn-success"><i class="icon-search"></i> 查询</button>
                    <a href="javascript:void(-1);" class="btn btn-success" id="addStrategyBtn">添加攻略</a>

                </div>
            </form>

            <table class="table table-striped table-hover">
                <thead>
                <tr>
                    <th>封面</th>
                    <th>攻略标题</th>
                    <th>副标题</th>
                    <th>所属地区</th>
                    <th>状态</th>
                    <th>操作</th>
                </tr>
                </thead>

                <#list pageInfo.list as entity>
                <tr>
                <#--entity_index+1 索引为0,id从1开始循环-->
                    <td><img src="${(entity.coverUrl)!}" width="40px"></td>
                    <td>${(entity.title)!}</td>
                    <td>${(entity.subTitle)!}</td>
                    <td>${(entity.place.name)!}</td>
                    <td>${(entity.stateName)!}</td>
                    <td>
                        <a href="javascript:void(0);" data-json='${(entity.jsonData)!}'
                           class="reviseBtn">修改</a>
                    </td>
                </tr>
                </#list>

            </table>
            <#include "../common/page.ftl">
        </div>
    </div>
</div>

<#--修改攻略模态框-->
<div id="travelCommendModal" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">编辑/增加</h4>
            </div>
            <div class="modal-body">
                <form id="imageForm" style="display: none" action="/image/upload.do" enctype="multipart/form-data"
                      method="post">
                    <input id="file" type="file" name="file">
                </form>

                <form id="editForm" class="form-horizontal" method="post" action="/strategy/saveOrUpdate.do"
                      enctype="multipart/form-data" style="margin: -3px 118px">
                    <input id="travelCommendId" type="hidden" name="id" value=""/>
                    <input id="travelId" type="hidden" name="travel.id" value=""/>
                    <input id="coverUrlId" type="hidden" name="coverUrl"/>  <#--封面的隐藏域-->
                    <div class="form-group">
                        <label class="col-sm-4 control-label">标题</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="title" name="title" placeholder="标题">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">副标题</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="subTitle" name="subTitle" placeholder="副标题">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">封面</label>
                        <div class="col-sm-6">
                            <img id="coverUrl" width="200px"/>
                            <input type="button" class="form-control" id="uploadImgBtn" value="选择图片" "/>

                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">所属地区</label>
                        <div class="col-sm-6">
                            <select id="place" class="form-control" autocomplete="off" name="place.id">
                                <#list regions as region>
                                    <option value="${region.id}">${region.name}</option>
                                </#list>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">状态</label>
                        <div class="col-sm-6">
                            <select id="state" class="form-control" autocomplete="off" name="state">
                                <option value="0">普通</option>
                                <option value="1">热门</option>
                                <option value="-1">禁用</option>
                            </select>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <a href="javascript:void(0);" class="btn btn-success" id="saveBtn" aria-hidden="true">保存</a>
                <a href="javascript:void(0);" class="btn" data-dismiss="modal" aria-hidden="true">关闭</a>
            </div>
        </div>
    </div>
</div>
</body>
</html>