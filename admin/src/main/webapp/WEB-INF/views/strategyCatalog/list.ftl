<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>叩丁狼部门</title>
    <#include "../common/header.ftl">

    <script>
        $(function () {

            //点击修改, 显示数据
            $('.reviseBtn').click(function () {
                var json = $(this).data("json");
                $('[name="id"]').val(json.id);
                $('#editForm [name=name]').val(json.name);
                $('input[name=sequence]').val(json.sequence);
                $('#state').val(json.state+"");
                $('#travelCommendModal').modal("show")
            });

            //新增数据   $("#editForm")[0].reset();
            $('#addStrategyBtn').click(function () {
                $('#editForm')[0].reset();
                $('#travelCommendModal').modal("show")
            });

            //提交表单
            $('#saveBtn').click(function () {
                $('#editForm').ajaxSubmit(function (data) {
                    successAlert(data);
                })
            });


        })

    </script>

</head>
<body>

<div class="container " style="margin-top: 20px">
    <#include "../common/top.ftl">
    <div class="row">
        <div class="col-sm-3">
            <#assign currentMenu="strategyCatalog"/>
            <#include "../common/menu.ftl">
        </div>
        <div class="col-sm-9">
            <div class="row">
                <div class="col-sm-12">
                    <h1 class="page-head-line">攻略分类管理</h1>
                </div>
            </div>
            <!--高级查询--->
            <form class="form-inline" id="searchForm" action="/strategyCatalog/list.do" method="post">
                <input type="hidden" name="currentPage" id="currentPage" value="1">
                <div class="form-group">
                    <label>攻略搜索</label>
                    <select id="strategySelect" name="strategyId" class="form-control form-control-chosen">
                    <#list strategies as strategy>
                        <option value="${strategy.id}">${strategy.title}</option>
                    </#list>
                    </select>
                    <script>
                        $('#strategySelect').val(${(qo.strategyId)!})
                    </script>
                </div>
                <div class="form-group">
                    <button id="query" type="submit" class="btn btn-success"><i class="icon-search"></i> 查询</button>
                    <a href="javascript:void(-1);" class="btn btn-success" id="addStrategyBtn">添加攻略</a>

                </div>
            </form>

            <table class="table table-striped table-hover">
                <thead>
                <tr>
                    <th>编号</th>
                    <th>分类名称</th>
                    <th>所属攻略</th>
                    <th>排序</th>
                    <th>状态</th>
                    <th>操作</th>
                </tr>
                </thead>

                <#list pageInfo.list as entity>
                <tr>
                <#--entity_index+1 索引为0,id从1开始循环-->
                    <td>${(entity_index+1)!}</td>
                    <td>${(entity.name)!}</td>
                    <td>${(entity.strategy.title)!}</td>
                    <td>${(entity.sequence)!}</td>
                    <td>${(entity.state?string("启用","禁用"))!}</td>
                    <td>
                        <a href="javascript:void(0);" data-json='${(entity.jsonData)!}'
                           class="reviseBtn">修改</a>
                    </td>
                </tr>
                </#list>

            </table>
            <#include "../common/page.ftl">
        </div>
    </div>
</div>

<#--攻略分类模态框-->
<div id="travelCommendModal" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">编辑/增加</h4>
            </div>
            <div class="modal-body">
                <form id="editForm" class="form-horizontal" method="post" action="/strategyCatalog/saveOrUpdate.do"
                      style="margin: -3px 118px">
                    <input id="strategyId" type="hidden" name="id" value=""/>

                    <div class="form-group">
                        <label class="col-sm-4 control-label">分类名称</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="name" name="name" placeholder="分类名称">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">所属攻略</label>
                        <div class="col-sm-6">
                            <select id="strategy" class="form-control" autocomplete="off" name="strategy.id"
                                    data-placeholder="请选择所属攻略">
                            <#list strategies as strategy>
                                <option value="${strategy.id}">${strategy.title}</option>
                            </#list>
                            </select>
                            <script>
                                $('#strategy').val(${(qo.strategyId)!})
                            </script>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">排序</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="sequence" name="sequence" placeholder="排序">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">状态</label>
                        <div class="col-sm-6">
                            <select id="state" class="form-control" autocomplete="off" name="state">
                                <option value="true">启用</option>
                                <option value="false">禁用</option>
                            </select>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <a href="javascript:void(0);" class="btn btn-success" id="saveBtn" aria-hidden="true">保存</a>
                <a href="javascript:void(0);" class="btn" data-dismiss="modal" aria-hidden="true">关闭</a>
            </div>
        </div>
    </div>
</div>
</body>
</html>