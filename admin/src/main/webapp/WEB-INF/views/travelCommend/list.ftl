<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>叩丁狼部门</title>
    <#include "../common/header.ftl">

    <script>
        $(function () {

            //点击修改, 显示数据
            $('.reviseBtn').click(function () {
                var json = $(this).data("json");
                $('[name="id"]').val(json.id);
                $('#editForm [name=title]').val(json.title);
                $('#editForm [name=subTitle]').val(json.subTitle);
                $('#editForm [name=schedule]').val(json.schedule);
                $('#coverUrl').attr("src", json.coverUrl);
                $('input [name=coverUrl]').val(json.coverUrl);
                $('#editForm [name=type]').val(json.type);
                console.log(json);
                $('.showTravel').attr("href","/travel/list.do?travelId="+json.travelId);
                $('#travelCommendModal').modal("show")
            });

            //提交表单
            $('#saveBtn').click(function () {
                $('#editForm').ajaxSubmit(function (data) {
                    successAlert(data);
                })
            });

            //绑定点击按钮和文件框
            $('#uploadImgBtn').click(function () {
                $('#file').click();
            });

            //文件框绑定改变事件
            $('#file').change(function () {
                if($(this).val()){
                    $('#imageForm').ajaxSubmit(function (data) {
                        $('#coverUrl').attr("src", data.url);
                        $('.modal-body [name=coverUrl]').val(data.url);

                    })
                }
            })
        })

    </script>

</head>
<body>

<div class="container " style="margin-top: 20px">
    <#include "../common/top.ftl">
    <div class="row">
        <div class="col-sm-3">
            <#assign currentMenu="travelCommend"/>
            <#include "../common/menu.ftl">
        </div>
        <div class="col-sm-9">
            <div class="row">
                <div class="col-sm-12">
                    <h1 class="page-head-line">游记推荐管理</h1>
                </div>
            </div>
            <!--高级查询--->
            <form class="form-inline" id="searchForm" action="/travelCommend/list.do" method="post">
                <input type="hidden" name="currentPage" id="currentPage" value="1">
                <div class="form-group">
                    <label for="keyword">关键字:</label>
                    <input type="text" class="form-control" id="keyword" name="keyword" value="${(qo.keyword)!}"
                           placeholder="请输入标题">
                </div>
                <div class="form-group">
                    <select id="type" class="form-control" autocomplete="off" name="type">
                        <option value="-1">全部</option>
                        <option value="1">每周推荐</option>
                        <option value="2">每月推荐</option>
                        <option value="3">攻略推荐</option>
                    </select>
                    <script>
                        $('#type').val("${(qo.type)!}")
                    </script>
                    <button id="query" type="submit" class="btn btn-success"><i class="icon-search"></i> 查询</button>
                </div>
            </form>

            <table class="table table-striped table-hover">
                <thead>
                <tr>
                    <th>封面</th>
                    <th>标题</th>
                    <th>副标题</th>
                    <th>推荐时间安排</th>
                    <th>推荐类型</th>
                    <th>操作</th>
                </tr>
                </thead>

                <#list pageInfo.list as entity>
                <tr>
                <#--entity_index+1 索引为0,id从1开始循环-->
                    <td><img src="${(entity.coverUrl)!}" width="40px"></td>
                    <td>${(entity.title)!}</td>
                    <td>${(entity.subTitle)!}</td>
                    <td>${(entity.schedule?string('yyyy-MM-dd'))!}</td>
                    <td>${(entity.typeName)!}</td>
                    <td>
                        <a href="javascript:void(0);" data-json='${(entity.jsonData)!}'
                           class="reviseBtn">修改</a>
                    </td>
                </tr>
                </#list>

            </table>
            <#include "../common/page.ftl">
        </div>
    </div>
</div>
<#--推荐模态框-->
<div id="travelCommendModal" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">编辑/增加</h4>
            </div>
            <div class="modal-body">
                <form id="imageForm" style="display: none" action="/image/upload.do" enctype="multipart/form-data" method="post">
                    <input id="file" type="file" name="file">
                </form>

                <form id="editForm" class="form-horizontal" method="post" action="/travelCommend/saveOrUpdate.do"
                      enctype="multipart/form-data" style="margin: -3px 118px">
                    <input id="travelCommendId" type="hidden" name="id" value=""/>
                    <input id="travelId" type="hidden" name="travel.id" value=""/>
                    <input id="coverUrlId" type="hidden" name="coverUrl"/>  <#--封面的隐藏域-->
                    <div class="form-group">
                        <label class="col-sm-4 control-label">标题</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="title" name="title" placeholder="标题">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">副标题</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="subTitle" name="subTitle" placeholder="副标题">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">封面</label>
                        <div class="col-sm-6">
                            <img id="coverUrl" width="200px"/>
                            <input type="button" class="form-control" id="uploadImgBtn" value="选择图片" "/>

                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">推荐时间</label>
                        <div class="col-sm-6">
                            <input type="text" id="schedule" name="schedule" class="form-control"
                                   onclick="WdatePicker()">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">推荐类型</label>
                        <div class="col-sm-6">
                            <select id="type" class="form-control" autocomplete="off" name="type">
                                <option value="1">每周推荐</option>
                                <option value="2">每月推荐</option>
                                <option value="3">攻略推荐</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group" style="text-align: center">
                        <a class="showTravel" target="_blank">点击查看游记文章明细</a>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <a href="javascript:void(0);" class="btn btn-success" id="saveBtn" aria-hidden="true">保存</a>
                <a href="javascript:void(0);" class="btn" data-dismiss="modal" aria-hidden="true">关闭</a>
            </div>
        </div>
    </div>
</div>
</body>
</html>