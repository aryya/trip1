<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="/bootstrap/css/bootstrap.css" type="text/css"/>
<link rel="stylesheet" href="/css/core.css" type="text/css"/>
<script type="text/javascript" src="/jquery/jquery-2.1.4.min.js"></script>
<script type="text/javascript" src="/bootstrap/js/bootstrap.js"></script>
<script type="text/javascript" src="/jquery/jquery.twbsPagination.min.js"></script>
<script type="text/javascript" src="/jquery/plugins/jquery.form.js"></script>
<script type="text/javascript" src="/jquery/plugins/jquery.bootstrap.min.js"></script>
<script src="/jquery/plugins/jquery.validate.min.js"></script>
<script src="/jquery/plugins/messages_cn.js"></script>
<script type="text/javascript" src="/js/My97DatePicker/WdatePicker.js"></script>
<script>

    $(function () {
        $.messager.model = {
            ok: {text: "确定"},
            cancel: {text: "取消"}
        };

        /*删除按钮绑定点击事件,弹出确认框*/

        $('.btn-delete').click(function (data) {
            var url = $(this).data("url");
            $.messager.confirm("温馨提示:", "亲,真的要删除吗", function () {
                $.get(url, function (data) {
                    successAlert(data);
                })
            })
        });
    });

    function successAlert(data) {
        if (data.success) {
            $.messager.alert("温馨提示:", "操作成功,2s后跳转");
            setTimeout(function () {
                window.location.reload();
            }, 2000)
        }
    }


</script>