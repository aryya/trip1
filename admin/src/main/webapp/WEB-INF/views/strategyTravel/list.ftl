<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>叩丁狼部门</title>
    <#include "../common/header.ftl">
    <script type="text/javascript" src="/js/ckeditor/ckeditor.js"></script>

    <script>
        $(function () {


            //点击修改, 显示数据
            $('.reviseBtn').click(function () {

                var json = $(this).data("json");
                if (json) {
                    $("[name='id']").val(json.id);
                    $('#editForm [name=title]').val(json.title);
                    $('#coverUrl').attr("src", json.coverUrl);
                    $("input[name='coverUrl']").val(json.coverUrl);
                    $("select[name='state']").val(json.state);
                    $("input[name='sequence']").val(json.sequence);

                    //大攻略回显
                    $('#strategySelect').val(json.strategyId);
                    $.get("/strategyCatalog/listByStrategyId.do", {strategyId: json.strategyId}, function (data) {
                        var temp = "";
                        $.each(data, function (index, ele) {
                            temp += '<option value="' + ele.id + '">' + ele.name + '</option>'
                        });
                        //攻略分类下拉框赋值
                        $('#catalogSelect').html(temp);
                        $('#catalogSelect').val(json.catalogId);

                    });

                    //回显内容框的文本
                    $.get('/strategyDetail/getContentById.do', {id: json.id}, function (data) {
                        ck.setData(data.content);
                    });
                }
                $('#travelCommendModal').modal("show")
            });

            //大攻略绑定change事件
            //获取攻略选中的值
            //发送ajax请求获取该攻略下的所有分类
            //循环拼接$.each
            //拼接option标签
            $('#strategySelect').change(function () {
                var strategyId = $(this).val()
                $.get("/strategyCatalog/listByStrategyId.do", {strategyId: strategyId}, function (data) {
                    console.log(data);
                    var temp = "";
                    $.each(data, function (index, ele) {
                        temp += '<option value="' + ele.id + '">' + ele.name + '</option>'
                    });
                    //攻略分类下拉框赋值
                    $('#catalogSelect').html(temp);

                })
            });


            //新增数据   $("#editForm")[0].reset();
            $('#addStrategyBtn').click(function () {
                //清空数据
                $('#editForm').val("");
                //清空文本
                ck.setData("");
                $('#travelCommendModal').modal("show")
            });

            //获取编辑器的内容,设置到表单组件中editor
            $('#saveBtn').click(function () {
                var data = ck.getData();
                $('#editor').html(data)
            });

            //提交表单
            $('#saveBtn').click(function () {
                $('#editForm').ajaxSubmit(function (data) {
                    successAlert(data);
                })
            });

            //绑定点击按钮和文件框
            $('#uploadImgBtn').click(function () {
                $('#file').click();
            });

            //文件框绑定改变事件
            $('#file').change(function () {
                if ($(this).val()) {
                    $('#imageForm').ajaxSubmit(function (data) {
                        $('#coverUrl').attr("src", data.url);
                        $('.modal-body [name=coverUrl]').val(data.url);

                    })
                }
            })
        })

    </script>

</head>
<body>

<div class="container " style="margin-top: 20px">
    <#include "../common/top.ftl">
    <div class="row">
        <div class="col-sm-3">
            <#assign currentMenu="strategyDetail"/>
            <#include "../common/menu.ftl">
        </div>
        <div class="col-sm-9">
            <div class="row">
                <div class="col-sm-12">
                    <h1 class="page-head-line">攻略管理</h1>
                </div>
            </div>
            <!--高级查询--->
            <form class="form-inline" id="searchForm" action="/strategyDetail/list.do" method="post">
                <input type="hidden" name="currentPage" id="currentPage" value="1">
                <div class="form-group">
                    <label for="keyword">关键字:</label>
                    <input type="text" class="form-control" id="keyword" name="keyword" value="${(qo.keyword)!}"
                           placeholder="请输入标题">
                </div>
                <div class="form-group">
                    <button id="query" type="submit" class="btn btn-success"><i class="icon-search"></i> 查询</button>
                    <a href="javascript:void(-1);" class="btn btn-success" id="addStrategyBtn">添加攻略文章</a>

                </div>
            </form>

            <table class="table table-striped table-hover">
                <thead>
                <tr>
                    <th>编号</th>
                    <th>标题</th>
                    <th>封面</th>
                    <th>发布时间</th>
                    <th>排序</th>
                    <th>攻略类别</th>
                    <th>状态</th>
                    <th>操作</th>
                </tr>
                </thead>

                <#list pageInfo.list as entity>
                <tr>
                <#--entity_index+1 索引为0,id从1开始循环-->
                    <td>${(entity_index+1)!}</td>
                    <td>${(entity.title)!}</td>
                    <td><img src="${(entity.coverUrl)!}" width="40px"></td>
                    <td>${(entity.releaseTime?string("yyyy-MM-dd HH:mm:ss"))!}</td>
                    <td>${(entity.sequence)!}</td>
                    <td>${(entity.catalog.name)!}</td>
                    <td>${(entity.stateName)!}</td>
                    <td>
                        <a href="javascript:void(0);" data-json='${(entity.jsonData)!}'
                           class="reviseBtn">修改</a>
                    </td>
                </tr>
                </#list>

            </table>
            <#include "../common/page.ftl">
        </div>
    </div>
</div>

<#--修改攻略模态框-->
<div id="travelCommendModal" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">编辑/增加</h4>
            </div>
            <div class="modal-body">
                <form id="imageForm" style="display: none" action="/image/upload.do" enctype="multipart/form-data"
                      method="post">
                    <input id="file" type="file" name="file">
                </form>

                <form id="editForm" class="form-horizontal" method="post" action="/strategyDetail/saveOrUpdate.do"
                      enctype="multipart/form-data" style="margin: -3px 118px">
                    <input id="travelCommendId" type="hidden" name="id" value=""/>
                    <input id="travelId" type="hidden" name="travel.id" value=""/>
                    <input id="coverUrlId" type="hidden" name="coverUrl"/>  <#--封面的隐藏域-->
                    <div class="form-group">
                        <label class="col-sm-4 control-label">标题</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="title" name="title" placeholder="标题">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">封面</label>
                        <div class="col-sm-6">
                            <img id="coverUrl" width="200px"/>
                            <input type="button" class="form-control" id="uploadImgBtn" value="选择图片" "/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-4 control-label">所属攻略</label>
                        <div class="col-sm-6">
                            <select id="strategySelect" class="form-control" autocomplete="off" name="strategy.id">
                            <#list strategies as strategy>
                                <option value="${strategy.id}">${strategy.title}</option>
                            </#list>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">攻略类别</label>
                        <div class="col-sm-6">
                            <select id="catalogSelect" class="form-control" autocomplete="off" name="catalog.id">

                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">排序</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="sequence" name="sequence" placeholder="排序">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">状态</label>
                        <div class="col-sm-6">
                            <select id="state" class="form-control" autocomplete="off" name="state">
                                <option value="0">草稿</option>
                                <option value="1">发布</option>
                                <option value="-1">禁用</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <textarea name="strategyContent.content" id="editor" rows="10" cols="80">
                        </textarea>
                        <script>
                            var ck = CKEDITOR.replace('editor');
                        </script>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <a href="javascript:void(0);" class="btn btn-success" id="saveBtn" aria-hidden="true">保存</a>
                <a href="javascript:void(0);" class="btn" data-dismiss="modal" aria-hidden="true">关闭</a>
            </div>
        </div>
    </div>
</div>
</body>
</html>