<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>叩丁狼部门</title>
    <#include "../common/header.ftl">
    <style>
        .modal-body img {
            width: 100%;
        }
    </style>
    <script>
        $(function () {

            //绑定查看按钮
            $('.lookBtn').click(function () {
                var id = $(this).data("id");
                $.get('/travel/getContentById.do', {id: id}, function (data) {
                    console.log((data.content));
                    $('.modal-body').html(data.content)

                });
                $('#contentModal').modal("show");
            });

            //状态值指定为1, 取消发布
            $('.cancelBtn').click(function () {
                var id = $(this).data("id");
                var state = $(this).data("state");
                var releaseTime = $(this).data("releaseTime");
                $.get("/travel/changeState.do", {id: id, state: state, releaseTime: releaseTime}, function (data) {
                    window.location.reload();
                })
            });

            //recommendBtn 绑定推荐按钮---Travel实体类
            $('.recommendBtn').click(function () {
                var json = $(this).data("json");
                $('#travelId').val(json.id);
                $('#editForm [name=title]').val(json.title);
                $('#coverUrl').attr("src", json.coverUrl);
                $('#coverUrlId').val(json.coverUrl);
                $('#travelCommendModal').modal("show")
            });

            //保存提交表单
            $('#saveBtn').click(function () {
                $('#editForm').ajaxSubmit(function (data) {
                    successAlert(data);
                })
            });
            //绑定点击按钮和文件框
            $('#uploadImgBtn').click(function () {
                $('#file').click();
            });

            //文件框绑定改变事件
            $('#file').change(function () {
                if($(this).val()){
                    $('#imageForm').ajaxSubmit(function (data) {
                        $('#coverUrl').attr("src", data.url);
                        $('.modal-body [name=coverUrl]').val(data.url);

                    })
                }
            })

        })

    </script>


</head>
<body>

<div class="container " style="margin-top: 20px">
    <#include "../common/top.ftl">
    <div class="row">
        <div class="col-sm-3">
            <#assign currentMenu="travel"/>
            <#include "../common/menu.ftl">
        </div>
        <div class="col-sm-9">
            <div class="row">
                <div class="col-sm-12">
                    <h1 class="page-head-line">已发布游记管理</h1>
                </div>
            </div>
            <!--高级查询--->
            <form class="form-inline" id="searchForm" action="/travel/list.do" method="post">
                <input type="hidden" name="currentPage" id="currentPage" value="1">
                <div class="form-group">
                    <label for="keyword">关键字:</label>
                    <input type="text" class="form-control" id="keyword" name="keyword" value="${(qo.keyword)!}"
                           placeholder="请输入地点/标题姓名">
                </div>
                <div class="form-group">
                    <input type="submit" id="btn_query" class="btn btn-default" value="查询">
                </div>
            </form>

            <table class="table table-striped table-hover">
                <thead>
                <tr>
                    <th>编号</th>
                    <th>标题</th>
                    <th>封面</th>
                    <th>地点</th>
                    <th>作者</th>
                    <th>发布时间</th>
                    <th>状态</th>
                </tr>
                </thead>

                <#list pageInfo.list as entity>
                <tr>
                <#--entity_index+1 索引为0,id从1开始循环-->
                    <td>${entity_index+1}</td>
                    <td>${(entity.title)!}</td>
                    <td><img src="${(entity.coverUrl)!}" width="40px"></td>
                    <td>${(entity.place.name)!}</td>
                    <td>${(entity.author.nickName)!}</td>
                    <td>${(entity.releaseTime?string("yyyy-MM-dd"))!}</td>
                    <td>${(entity.stateName)!}</td>

                    <td>
                        <a href="javascript:void(0);" class="lookBtn" data-id="${entity.id}">查看游记</a>
                    </td>
                    <td>
                        <a href="javascript:void(0);" data-state="1" data-id="${entity.id}"
                           class="cancelBtn">取消发布</a>
                    </td>
                    <td>
                        <a href="javascript:void(0);" data-state="-1" data-json='${(entity.jsonData)!}'
                           class="recommendBtn">推荐</a>
                    </td>
                </tr>
                </#list>
            </table>
            <#include "../common/page.ftl">
        </div>
    </div>
</div>
<#--推荐模态框-->
<div id="travelCommendModal" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">编辑/增加</h4>
            </div>
            <div class="modal-body">
                <form id="imageForm" style="display: none" action="/image/upload.do" enctype="multipart/form-data"
                      method="post">
                    <input id="file" type="file" name="file">
                </form>

                <form id="editForm" class="form-horizontal" method="post" action="/travelCommend/saveOrUpdate.do"
                      enctype="multipart/form-data" style="margin: -3px 118px">
                    <input id="travelCommendId" type="hidden" name="id" value=""/>
                    <input id="travelId" type="hidden" name="travel.id" value=""/>
                    <input id="coverUrlId" type="hidden" name="coverUrl"/>  <#--封面的隐藏域-->
                    <div class="form-group">
                        <label class="col-sm-4 control-label">标题</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="title" name="title" placeholder="标题">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">副标题</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="subTitle" name="subTitle" placeholder="副标题">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">封面</label>
                        <div class="col-sm-6">
                            <img id="coverUrl" width="200px"/>
                            <input type="button" class="form-control" id="uploadImgBtn" value="选择图片" "/>

                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">推荐时间</label>
                        <div class="col-sm-6">
                            <input type="text" id="schedule" name="schedule" class="form-control"
                                   onclick="WdatePicker()">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">推荐类型</label>
                        <div class="col-sm-6">
                            <select id="type" class="form-control" autocomplete="off" name="type">
                                <option value="1">每周推荐</option>
                                <option value="2">每月推荐</option>
                                <option value="3">攻略推荐</option>
                            </select>
                        </div>
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <a href="javascript:void(0);" class="btn btn-success" id="saveBtn" aria-hidden="true">保存</a>
                <a href="javascript:void(0);" class="btn" data-dismiss="modal" aria-hidden="true">关闭</a>
            </div>
        </div>
    </div>
</div>
<#--文章内容框-->
<div id="contentModal" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">查看内容</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-foot">
                <a href="javascript:void(0);" class="btn" data-dismiss="modal" aria-hidden="true">关闭</a>
            </div>
        </div>
    </div>
</div>
</body>
</html>