<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>叩丁狼部门</title>
    <#include "../common/header.ftl">
    <style>
        .modal-body img {
            width: 100%;
        }
    </style>

    <script>
        $(function () {

            //绑定发布和拒绝
            $('.changeStateBtn').click(function () {
                var id = $(this).data("id");
                var state = $(this).data("state");
                var releaseTime = $(this).data("releaseTime");
                $.get("/travel/changeState.do", {id: id, state: state,releaseTime:releaseTime}, function (data) {
                    window.location.reload();
                })
            });


            //绑定查看按钮
            $('.lookBtn').click(function () {
                var id = $(this).data("id");
                $.get('/travel/getContentById.do', {id: id}, function (data) {
                    $('.modal-body').html(data.content);
                });
                $('#contentModal').modal("show");
            })
        })

    </script>


</head>
<body>

<div class="container " style="margin-top: 20px">
    <#include "../common/top.ftl">
    <div class="row">
        <div class="col-sm-3">
            <#assign currentMenu="audit"/>
            <#include "../common/menu.ftl">
        </div>
        <div class="col-sm-9">
            <div class="row">
                <div class="col-sm-12">
                    <h1 class="page-head-line">待审核游记管理</h1>
                </div>
            </div>
            <!--高级查询--->
            <form class="form-inline" id="searchForm" action="/travel/auditList.do" method="post">
                <input type="hidden" name="currentPage" id="currentPage" value="1">
                <div class="form-group">
                    <label for="keyword">关键字:</label>
                    <input type="text" class="form-control" id="keyword" name="keyword" value="${(qo.keyword)!}"
                           placeholder="请输入地点/标题姓名">
                </div>
                <div class="form-group">
                    <select id="auditState" class="form-control" autocomplete="off" name="state">
                        <option value="-2">全部</option>
                        <option value="1">待审核</option>
                        <option value="-1">已拒绝</option>
                    </select>
                    <script>
                        $('#auditState').val("${qo.state}")
                    </script>
                    <input type="submit" id="btn_query" class="btn btn-default" value="查询">
                </div>
            </form>

            <table class="table table-striped table-hover">
                <thead>
                <tr>
                    <th>编号</th>
                    <th>标题</th>
                    <th>封面</th>
                    <th>地点</th>
                    <th>作者</th>
                    <th>状态</th>
                </tr>
                </thead>

                <#list pageInfo.list as entity>
                <tr>
                <#--entity_index+1 索引为0,id从1开始循环-->
                    <td>${entity_index+1}</td>
                    <td>${(entity.title)!}</td>
                    <td><img src="${(entity.coverUrl)!}" width="40px"></td>
                    <td>${(entity.place.name)!}</td>
                    <td>${(entity.author.nickName)!}</td>
                    <td>${(entity.stateName)!}</td>
                    <td>
                        <a href="javascript:void(0);" data-state="2" data-id="${entity.id}"
                           class="changeStateBtn">发布</a>
                    </td>
                    <td>
                        <a href="javascript:void(0);" data-state="-1" data-id="${entity.id}"
                           class="changeStateBtn">拒绝</a>
                    </td>
                    <td>
                        <a href="javascript:void(0);" class="lookBtn" data-id="${entity.id}">查看文章</a>
                    </td>

                </tr>
                </#list>

            </table>
            <#include "../common/page.ftl">
        </div>
    </div>
</div>

<#--文章内容框-->
<div id="contentModal" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">查看内容</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-foot">
                <a href="javascript:void(0);"  class="btn" data-dismiss="modal" aria-hidden="true">关闭</a>
            </div>
        </div>
    </div>
</div>
</body>
</html>