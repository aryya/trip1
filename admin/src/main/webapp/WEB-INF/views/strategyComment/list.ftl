<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>叩丁狼部门</title>
    <#include "../common/header.ftl">
    <script type="text/javascript" src="/js/ckeditor/ckeditor.js"></script>

    <script>

        $(function () {
            $('.stateBtn').click(function () {
                var json = $(this).data("json");
                $('#strategyCommentId').val(json.id);
                $('#state').val(json.state);
                $('#strategyCommentModal').modal("show")

            });
            //提交表单
            $('#saveBtn').click(function () {
                $('#editForm').ajaxSubmit(function (data) {
                    successAlert(data);
                })
            });

        })
    </script>

</head>
<body>

<div class="container " style="margin-top: 20px">
    <#include "../common/top.ftl">
    <div class="row">
        <div class="col-sm-3">
            <#assign currentMenu="strategyComment"/>
            <#include "../common/menu.ftl">
        </div>
        <div class="col-sm-9">
            <div class="row">
                <div class="col-sm-12">
                    <h1 class="page-head-line">攻略评论管理</h1>
                </div>
            </div>
            <form class="form-inline" id="searchForm" action="/strategyComment/list.do" method="post">
                <input type="hidden" name="currentPage" id="currentPage" value="1">
            </form>
            <table class="table table-striped table-hover">
                <thead>
                <tr>
                    <th>编号</th>
                    <th>评论者</th>
                    <th>时间</th>
                    <th>图片</th>
                    <th>星星</th>
                    <th>攻略</th>
                    <th>状态</th>
                    <th>内容</th>
                    <th>操作</th>
                </tr>
                </thead>

                <#list pageInfo.list as entity>
                <tr>
                <#--entity_index+1 索引为0,id从1开始循环-->
                    <td>${(entity_index+1)!}</td>
                    <td>${(entity.user.nickName)!}</td>
                    <td>${(entity.createTime?string("yyyy-MM-dd HH:mm:ss"))!}</td>
                    <td><img src="${(entity.imgUrls)!}" width="40px"></td>
                    <td>${(entity.starNum)!}</td>
                    <td>${(entity.strategy.title)!}</td>
                    <td>${(entity.stateName)!}</td>
                    <td>${(entity.content)!}</td>
                    <td>
                        <a href="javascript:void(0);" class="stateBtn" data-json='${(entity.jsonData)!}'>设置状态</a>
                    </td>
                </tr>
                </#list>

            </table>
            <#include "../common/page.ftl">
        </div>
    </div>
</div>

<div id="strategyCommentModal" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">修改状态</h4>
            </div>
            <div class="modal-body">
                <form id="editForm" class="form-horizontal" method="post" action="/strategyComment/updateStatus.do"
                      style="margin: -3px 118px">
                    <input id="strategyCommentId" type="hidden" name="id" value=""/>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">状态</label>
                        <div class="col-sm-6">
                            <select id="state" class="form-control" autocomplete="off" name="state">
                                <option value="-1">禁用</option>
                                <option value="0">正常</option>
                                <option value="1">推荐</option>
                            </select>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <a href="javascript:void(0);" class="btn btn-success" id="saveBtn" aria-hidden="true">保存</a>
                <a href="javascript:void(0);" class="btn" data-dismiss="modal" aria-hidden="true">关闭</a>
            </div>
        </div>
    </div>
</div>
</body>
</html>