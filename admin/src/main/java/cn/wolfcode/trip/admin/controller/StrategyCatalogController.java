package cn.wolfcode.trip.admin.controller;

import cn.wolfcode.trip.base.domain.StrategyCatalog;
import cn.wolfcode.trip.base.query.StrategyCatalogQueryObject;
import cn.wolfcode.trip.base.service.IStrategyCatalogService;
import cn.wolfcode.trip.base.service.IStrategyService;
import cn.wolfcode.trip.base.util.JsonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import java.util.List;

@Controller
@RequestMapping("/strategyCatalog")
public class StrategyCatalogController {

    @Autowired
    private IStrategyCatalogService catalogService;
    @Autowired
    private IStrategyService strategyService;

    @RequestMapping("/list")
    public String list(@ModelAttribute("qo") StrategyCatalogQueryObject qo, Model model) {
        //查询所有的攻略title
        model.addAttribute("strategies", strategyService.selectAll());
        model.addAttribute("pageInfo", catalogService.selectForList(qo));
        return "strategyCatalog/list";

    }

    @ResponseBody
    @RequestMapping("/saveOrUpdate")
    public JsonResult saveOrUpdate(StrategyCatalog strategyCatalog) {
        catalogService.saveOrUpdate(strategyCatalog);
        return new JsonResult();
    }

    //通过攻略的id查询旗下的所有分类(二级联动的下拉框)
    @ResponseBody
    @RequestMapping("/listByStrategyId")
    public List<StrategyCatalog> listByStrategyId(Long strategyId) {

        return catalogService.listByStrategyId(strategyId);

    }

}
