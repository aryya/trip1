package cn.wolfcode.trip.admin.controller;


import cn.wolfcode.trip.base.domain.Daily;
import cn.wolfcode.trip.base.domain.DailyContent;
import cn.wolfcode.trip.base.domain.StrategyContent;
import cn.wolfcode.trip.base.domain.StrategyDetail;
import cn.wolfcode.trip.base.query.DailyQueryObject;
import cn.wolfcode.trip.base.service.IDailyContentService;
import cn.wolfcode.trip.base.service.IDailyService;
import cn.wolfcode.trip.base.util.JsonResult;
import cn.wolfcode.trip.base.util.UploadUtil;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@Controller
@RequestMapping("/daily")
public class DailyController {

    @Autowired
    private IDailyService dailyService;
    @Autowired
    private IDailyContentService dailyContentService;


    @RequestMapping("/list")
    public String  listAll(@ModelAttribute("qo") DailyQueryObject qo,Model m){
        m.addAttribute("pageInfo",dailyService.query(qo));
        return "daily/list";
   }


    @RequestMapping("/getDailyContent")
    @ResponseBody
    public DailyContent getContentById(Long id) {
        return dailyContentService.getDailyContent(id);

    }

    @ResponseBody
    @RequestMapping("/saveOrUpdate")
    public JsonResult saveOrUpdate(Daily daily, MultipartFile file) {
        //设置封面路径
        if (file != null && file.getSize() > 0) {
            String url = UploadUtil.upload(file, UploadUtil.PATH + "/upload");
            daily.setCoverUrl(url);
        }
        dailyService.saveOrUpdate(daily);
        return new JsonResult();
    }

}
