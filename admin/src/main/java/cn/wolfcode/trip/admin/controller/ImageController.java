package cn.wolfcode.trip.admin.controller;

import cn.wolfcode.trip.base.util.UploadUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/image")
public class ImageController {

    //插件
    @RequestMapping("/upload")
    @ResponseBody
    public Map upload(MultipartFile file) {
        HashMap<Object, Object> map = new HashMap<>();
        try {
            String url = UploadUtil.upload(file, UploadUtil.PATH + "/upload");
            map.put("status", 1);
            map.put("url", url);
        } catch (Exception e) {
            e.printStackTrace();
            map.put("status", 0);
            map.put("msg", "上传失败");
        }
        return map;
    }

    //用于插件CKeditor
    @PostMapping
    @ResponseBody
    public Map uploadCKImage(MultipartFile upload) {
        HashMap<Object, Object> map = new HashMap<>();
        try {
            String url = UploadUtil.upload(upload, UploadUtil.PATH + "/upload");
            map.put("uploaded", 1);
            map.put("url", url);
        } catch (Exception e) {
            e.printStackTrace();
            map.put("uploaded", 0);
            HashMap temp = new HashMap();
            temp.put("message", "上传失败");
            map.put("error", temp);
        }
        return map;
    }

}
