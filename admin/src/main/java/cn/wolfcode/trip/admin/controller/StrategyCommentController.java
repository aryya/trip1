package cn.wolfcode.trip.admin.controller;

import cn.wolfcode.trip.base.domain.StrategyComment;
import cn.wolfcode.trip.base.query.StrategyCommentQueryObject;
import cn.wolfcode.trip.base.service.IStrategyCommentService;
import cn.wolfcode.trip.base.util.JsonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/strategyComment")
public class StrategyCommentController {

    @Autowired
    private IStrategyCommentService commentService;

    @RequestMapping("/list")
    public String list(@ModelAttribute("qo") StrategyCommentQueryObject qo, Model model) {
        model.addAttribute("pageInfo", commentService.selectForList(qo));
        return "strategyComment/list";
    }

    @RequestMapping("/updateStatus")
    @ResponseBody
    public JsonResult updateStausById(StrategyComment strategyComment) {
        commentService.updateStausById(strategyComment);
        return new JsonResult();
    }
}
