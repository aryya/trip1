package cn.wolfcode.trip.admin.controller;

import cn.wolfcode.trip.base.domain.TravelCommend;
import cn.wolfcode.trip.base.query.TravelCommendQueryObject;
import cn.wolfcode.trip.base.service.ITravelCommendService;
import cn.wolfcode.trip.base.util.JsonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/travelCommend")
public class TravelCommendController {

    @Autowired
    private ITravelCommendService travelCommendService;

    @RequestMapping("/list")
    public String list(@ModelAttribute("qo") TravelCommendQueryObject qo, Model model) {

        model.addAttribute("pageInfo", travelCommendService.selectForList(qo));
        return "travelCommend/list";

    }

    @ResponseBody
    @RequestMapping("saveOrUpdate")
    public JsonResult saveOrUpdate(TravelCommend travelCommend) {

        travelCommendService.saveOrUpdate(travelCommend);
        return new JsonResult();
    }
}
