package cn.wolfcode.trip.admin.controller;

import cn.wolfcode.trip.base.domain.Travel;
import cn.wolfcode.trip.base.domain.TravelContent;
import cn.wolfcode.trip.base.query.TravelQueryObject;
import cn.wolfcode.trip.base.service.ITravelService;
import cn.wolfcode.trip.base.util.JsonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/travel")
public class TravelController {

    @Autowired
    public ITravelService travelService;


    /**
     * 已发布游记页面
     *
     * @return
     */
    @RequestMapping("list")
    public String list(Model model, TravelQueryObject qo) {
        qo.setOrderBy("releaseTime desc");

        //设定状态为已发布的信息
        qo.setState(Travel.STATE_POST);
        model.addAttribute("pageInfo", travelService.queryTravel(qo));
        return "travel/releaseList";

    }


    /**
     * 待审核游记页面
     *
     * @param qo
     * @param model
     * @return
     */
    @RequestMapping("/auditList")
    public String query(@ModelAttribute("qo") TravelQueryObject qo, Model model) {
        //作用:点击进入界面时设置全部为审核状态
        if (qo.getState() == null) {
            qo.setState(Travel.STATE_AUDIT);
        }
        //根据用户提交的最早更新时间来升序
        qo.setOrderBy("lastUpdateTime asc");

        //公开的游记
        qo.setIsPublic(true);

        model.addAttribute("pageInfo", travelService.queryTravel(qo));
        return "travel/list";
    }

    /**
     * 发布按钮,拒绝按钮
     * 改变状态值
     *
     * @return
     */
    @ResponseBody
    @RequestMapping("/changeState")
    public JsonResult changeState(Travel travel) {
        travelService.changeState(travel);
        return new JsonResult();
    }

    /**
     * TravelContent 使用同一个controller
     * 获取模态框中的文章内容
     * @param id
     * @return TravelContent
     */
    @RequestMapping("/getContentById")
    @ResponseBody
    public TravelContent getContentById(Long id) {
        return travelService.getContentById(id);
    }
}
