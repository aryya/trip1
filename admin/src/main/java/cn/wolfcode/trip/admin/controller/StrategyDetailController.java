package cn.wolfcode.trip.admin.controller;

import cn.wolfcode.trip.base.domain.StrategyContent;
import cn.wolfcode.trip.base.domain.StrategyDetail;
import cn.wolfcode.trip.base.query.StrategyDetailQueryObject;
import cn.wolfcode.trip.base.service.IStrategyDetailService;
import cn.wolfcode.trip.base.service.IStrategyService;
import cn.wolfcode.trip.base.util.JsonResult;
import cn.wolfcode.trip.base.util.UploadUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;


@Controller
@RequestMapping("/strategyDetail")
public class StrategyDetailController {

    @Autowired
    private IStrategyDetailService detailService;
    @Autowired
    private IStrategyService strategyService;


    /**
     * 显示页面
     *
     * @param qo
     * @param model
     * @return
     */
    @RequestMapping("/list")
    public String list(@ModelAttribute("qo") StrategyDetailQueryObject qo, Model model) {
        //查询所有的攻略
        model.addAttribute("strategies", strategyService.selectAll());

        model.addAttribute("pageInfo", detailService.selectForList(qo));
        return "strategyDetail/list";

    }

    /**
     * 更新或保存
     *
     * @param strategyDetail
     * @param file
     * @return
     */
    @ResponseBody
    @RequestMapping("/saveOrUpdate")
    public JsonResult saveOrUpdate(StrategyDetail strategyDetail, MultipartFile file) {
        //设置封面路径
        if (file != null && file.getSize() > 0) {
            String url = UploadUtil.upload(file, UploadUtil.PATH + "/upload");
            strategyDetail.setCoverUrl(url);
        }
        detailService.saveOrUpdate(strategyDetail);
        return new JsonResult();
    }

    @RequestMapping("/getContentById")
    @ResponseBody
    public StrategyContent getContentById(Long id) {
        return detailService.getContentById(id);

    }


}
