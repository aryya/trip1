$(function () {
    var signFun = function () {
        var user = JSON.parse(window.sessionStorage.getItem('user'));

        var dateArray = []; // 假设已经签到的

        $.get("/Sign/getMonthSign", {currentUserId: user.id, currentMonth: new Date().getMonth() + 1}, function (data) {
            console.log(data);
            if (data.length != 0) {
                console.log(data);
                //查询
                $.merge(dateArray, data);
                console.log(dateArray);
            }

            var $dateBox = $("#js-qiandao-list"),
                $currentDate = $(".current-date"),
                $qiandaoBnt = $("#js-just-qiandao"),
                _html = '',
                _handle = true,
                myDate = new Date();
            $currentDate.text(myDate.getFullYear() + '年' + parseInt(myDate.getMonth() + 1) + '月' + myDate.getDate() + '日');

            var monthFirst = new Date(myDate.getFullYear(), parseInt(myDate.getMonth()), 1).getDay();

            var d = new Date(myDate.getFullYear(), parseInt(myDate.getMonth() + 1), 0);
            var totalDay = d.getDate(); //获取当前月的天数

            for (var i = 0; i < 42; i++) {
                _html += ' <li><div class="qiandao-icon"></div></li>'
            }
            $dateBox.html(_html); //生成日历网格

            var $dateLi = $dateBox.find("li");
            for (var i = 0; i < totalDay; i++) {
                $dateLi.eq(i + monthFirst).addClass("date" + parseInt(i + 1));
                for (var j = 0; j < dateArray.length; j++) {
                    if (i == dateArray[j]) {
                        $dateLi.eq(i + monthFirst).addClass("qiandao");
                    }
                }
            } //生成当月的日历且含已签到

            $(".date" + myDate.getDate()).addClass('able-qiandao');

            if ($dateLi.eq(dateArray[dateArray.length - 1] + monthFirst).hasClass("qiandao") &&
                new Date().getDate() == dateArray[dateArray.length - 1] + 1) {
                $qiandaoBnt.addClass('actived');
                _handle = false;
            }


            $dateBox.on("click", "li", function () {
                if (!($dateLi.eq(dateArray[dateArray.length - 1] + monthFirst).hasClass("qiandao") &&
                        new Date().getDate() == dateArray[dateArray.length - 1] + 1)) {
                    if ($(this).hasClass('able-qiandao') && _handle) {
                        $(this).addClass('qiandao');

                    }
                }

            }); //签到

            $qiandaoBnt.on("click", function () {
                if (_handle) {

                    $.post("/Sign/creatNewSign", {currentUserId: user.id}, function (data) {
                        $("#SignDays").html(data.days);
                        //增加积分
                        $("#integral").html(1+1*data.days+'<em>积分</em>');

                        $.post("/Sign/addIntegral",{currentUserId: user.id,integral:1+1*data.days},function (data) {

                        });
                        qiandaoFun();
                    });


                    qiandaoFun();
                }
            }); //签到

            function qiandaoFun() {
                $qiandaoBnt.addClass('actived');
                openLayer("qiandao-active", qianDao);
                _handle = false;
            }

            function qianDao() {
                $(".date" + myDate.getDate()).addClass('qiandao');
            }
        });


    }();

    function openLayer(a, Fun) {

        $('.' + a).fadeIn(Fun)
    } //打开弹窗

    var closeLayer = function () {
        $("body").on("click", ".close-qiandao-layer", function () {
            $(this).parents(".qiandao-layer").fadeOut()
        })
    }(); //关闭弹窗

    $("#js-qiandao-history").on("click", function () {
        openLayer("qiandao-history-layer", myFun);

        function myFun() {
            console.log(1)
        } //打开弹窗返回函数
    })

})
