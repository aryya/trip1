package cn.wolfcode.trip.app.controller;

import cn.wolfcode.trip.base.util.UploadUtil;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import sun.misc.BASE64Decoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@RestController
@RequestMapping("/images")
public class ImageController {

    //插件
    @PostMapping
    public Map upload(MultipartFile file) {
        HashMap<Object, Object> map = new HashMap<>();
        try {
            String url = UploadUtil.upload(file, UploadUtil.PATH + "/upload");
            map.put("status", 1);
            map.put("url", url);
        } catch (Exception e) {
            e.printStackTrace();
            map.put("status", 0);
            map.put("msg", "上传失败");
        }
        return map;
    }

    @RequestMapping(value = "/sc", method = RequestMethod.POST)
    @ResponseBody
    public String sc(HttpServletResponse response, HttpServletRequest request, String sj) throws Exception {
        GenerateImage(sj);
        return "sxt";
    }

    // base64字符串转化成图片
    public static boolean GenerateImage(String imgStr) { // 对字节数组字符串进行Base64解码并生成图片
        if (imgStr == null) // 图像数据为空
            return false;
        try {
            // Base64解码
            BASE64Decoder decoder = new BASE64Decoder();
            byte[] bytes = decoder.decodeBuffer(imgStr);

            // byte[] bytes = Base64.decodeBase64(imgStr);   //或用Base64 解   org.apache.commons.codec.binary.Base64;

            for (int i = 0; i < bytes.length; ++i) {
                if (bytes[i] < 0) {// 调整异常数据
                    bytes[i] += 256;
                }
            }
            //获取随机生成的字符串
            String uuid = UUID.randomUUID().toString();
            String fileName = uuid + ".jpeg";
            // 生成jpeg图片
            String imgFilePath = "c://trip/upload/" + fileName;// 新生成的图片
            OutputStream out = new FileOutputStream(imgFilePath);
            out.write(bytes);
            out.flush();
            out.close();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

}
