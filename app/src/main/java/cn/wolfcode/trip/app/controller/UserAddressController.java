package cn.wolfcode.trip.app.controller;


import cn.wolfcode.trip.base.domain.UserAddress;
import cn.wolfcode.trip.base.service.IUserAddressService;
import cn.wolfcode.trip.base.util.JsonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

//收货地址资源控制器
@RestController
@RequestMapping("/addresses")
public class UserAddressController {

    @Autowired
    private IUserAddressService userAddressService;

    /**
     * 1.新增收货地址
     * 资源：/addresses
     * 动作：POST
     * 参数：详情看数据库表字段
     */
    @PostMapping
    public Object saveAddresses(UserAddress entity) {
        JsonResult result = new JsonResult();
        try {
            userAddressService.save(entity);
        } catch (Exception e) {
            result.mark(e.getMessage());
        }
        return result;
    }
}
