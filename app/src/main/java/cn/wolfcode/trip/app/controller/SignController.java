package cn.wolfcode.trip.app.controller;

import cn.wolfcode.trip.base.domain.Sign;
import cn.wolfcode.trip.base.query.SignQueryObject;
import cn.wolfcode.trip.base.service.ISignService;
import cn.wolfcode.trip.base.util.JsonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.xml.crypto.Data;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

@RestController
@RequestMapping("/Sign")
public class SignController {
    @Autowired
    private ISignService signService;

    /**
     * 查询当前月的签到信息
     * @param qo 查询条件
     * @return 当前月签到的信息集合
     */
    @GetMapping("/getMonthSign")
    public List getMonthSign(SignQueryObject qo) {
        List<Sign> signList = signService.getMonthSign(qo);
        List sign = new ArrayList();
        Calendar calendar = Calendar.getInstance();
        for (Sign s : signList) {
            calendar.setTime(s.getSignDate());
            sign.add(calendar.get(Calendar.DAY_OF_MONTH)-1);
        }
        return sign;
    }

    /**
     * 添加新的签到信息
     * @param qo
     * @return
     */
    @PostMapping("/creatNewSign")
    public Sign creatNewSign(SignQueryObject qo){
        Sign sign = signService.creatNewSign(qo);
        return sign;
    }

    /**
     * 签到添加积分
     * @param currentUserId 当前用户的id
     * @param integral 签到的积分增加的
     * @return 签到对象
     */
    @PostMapping("/addIntegral")
    public Sign addIntegral(Long currentUserId,Integer integral){

        signService.addIntegral(currentUserId,integral);
        return new Sign();
    }

}
