package cn.wolfcode.trip.app.controller;

import cn.wolfcode.trip.base.domain.GoodMe;
import cn.wolfcode.trip.base.query.GoodMeQueryObject;
import cn.wolfcode.trip.base.query.QueryObject;
import cn.wolfcode.trip.base.service.IGoodMeService;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 点赞查询
 */
@RestController
@RequestMapping("/good")
public class GoodMeController {
    @Autowired
    private IGoodMeService goodMeService;

    /**
     * 获取当前未读的点赞数
     *
     * @param currentId 当前用户的id
     * @return 未读的点赞数
     */
    @RequestMapping("/{currentId}/getGoodMeCount")
    public int getGoodMeCount(@PathVariable Long currentId) {
        return goodMeService.getGoodMeCount(currentId);
    }

    /**
     * 获取所有的游记点赞数
     *
     * @param qo 当前用户的id1
     * @return 点赞的信息
     */
    @RequestMapping("/getAllTravelGoode")
    public PageInfo getAllTravelGoode(GoodMeQueryObject qo) {
        qo.setOrderBy("g.createTime DESC");
        PageInfo pageInfo = goodMeService.getAllTravelGoode(qo);
        List<GoodMe> list = pageInfo.getList();
        for (GoodMe goodMe : list) {
            goodMeService.updateState(goodMe);
        }
        return pageInfo;
    }

    /**
     * 获取所有的评论点赞数
     *
     * @param qo 当前用户的id1
     * @return 点赞的信息
     */
    @RequestMapping("/getAllCommentGoode")
    public PageInfo getAllCommentGoode(GoodMeQueryObject qo) {
        qo.setOrderBy("g.createTime desc");
        PageInfo pageInfo = goodMeService.getAllCommentGoode(qo);
        List<GoodMe> list = pageInfo.getList();
        for (GoodMe goodMe : list) {
            goodMeService.updateState(goodMe);
        }

        return pageInfo;
    }


}
