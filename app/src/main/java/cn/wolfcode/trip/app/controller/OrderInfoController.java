package cn.wolfcode.trip.app.controller;

import cn.wolfcode.trip.base.service.IOrderInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

//订单资源控制器
@RestController
@RequestMapping("/orders")
public class OrderInfoController {

    @Autowired
    private IOrderInfoService orderInfoService;

    @GetMapping
    public Object query() {
        return null;
    }
}
