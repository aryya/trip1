package cn.wolfcode.trip.app.controller;

import cn.wolfcode.trip.base.query.QueryObject;
import cn.wolfcode.trip.base.service.IExchangeRateService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;

@RestController
@RequestMapping("/exchangeRates")
public class ExchangeRateController {
    @Autowired
    private IExchangeRateService rateService;

    @GetMapping
    public PageInfo listAll(QueryObject qo){
        qo.setPageSize(10);
        PageInfo pageInfo = rateService.listAll(qo);
        return pageInfo;
    }

    @GetMapping("/{id}")
    public BigDecimal selectinsidePriceById(@PathVariable Long id){
       return rateService.selectInSidePriceById(id);
    }

    @GetMapping("/count")
    public String countRates(Long exchangeRateId,BigDecimal money){
        BigDecimal decimal = rateService.countCountRates(exchangeRateId,money);
        String minName=rateService.selectRatesMinName(exchangeRateId);
        String s = decimal.toString();
        return minName+":"+s;
    }
}
