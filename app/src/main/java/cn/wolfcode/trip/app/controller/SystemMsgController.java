package cn.wolfcode.trip.app.controller;

import cn.wolfcode.trip.base.domain.SystemMsg;
import cn.wolfcode.trip.base.service.impl.ISystemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/systemMsg")
public class SystemMsgController {
    @Autowired
    private ISystemService systemService;

    /**
     * 查询所有的系统信息
     * @param currentId
     * @return
     */
    @RequestMapping("/{currentId}/getAllSytemMsgByCurrentId")
    public List<SystemMsg> getAllSytemMsgByCurrentId(@PathVariable Long currentId) {
        List<SystemMsg> msgList = systemService.getAllSytemMsgByCurrentId(currentId);
        for (SystemMsg systemMsg : msgList) {
            systemMsg.setState(false);
            systemService.updateState(systemMsg);
        }
        return msgList;
    }

    /**
     * 查询当前用户未读的系统消息
     * @param currentId
     * @return
     */
    @GetMapping("/{currentId}/getSystemMsg")
    public int getSystemMsgCount(@PathVariable Long currentId){
        return systemService.getSystemMsgCount(currentId);
    }



}
