package cn.wolfcode.trip.app.controller;

import cn.wolfcode.trip.base.domain.CommentReply;
import cn.wolfcode.trip.base.domain.CommunityReply;
import cn.wolfcode.trip.base.domain.StrategyComment;
import cn.wolfcode.trip.base.domain.TravelContent;
import cn.wolfcode.trip.base.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/commentTips")
public class CommentTipsController {
    @Autowired
    private IUserService userService;
    @Autowired
    private ICommentReplyService replyService;
    @Autowired
    private ITravelCommentService commentService;

    /**
     * 查询未读的评论数
     * @param userId
     * @return
     */
    @GetMapping("/count/{userId}")
    public int getCommentTips(@PathVariable Long userId){
        return userService.getCommentTips(userId);

    }
    /**
     * 查询所有未读的攻略评价
     */

    @GetMapping("/strategy/{userId}")
    public List<CommentReply> listStrategyCommentByUserId(@PathVariable Long userId){
        List<CommentReply> commentReplies = replyService.listStrategyCommentByUserId(userId);
        return commentReplies;
    }
    /**
     * 查询所有未读的游记评价
     */

    @GetMapping("/travel/{userId}")
    public List<TravelContent> listTravelCommentByUserId(@PathVariable Long userId){
        List<TravelContent> TravelContents = commentService.listTravelCommentByUserId(userId);
        return TravelContents;
    }
    /**
     */
    @GetMapping("/delete/{userId}")
    public void deleteNum(@PathVariable Long userId){
        userService.deleteCommentNum(userId);
    }
}
