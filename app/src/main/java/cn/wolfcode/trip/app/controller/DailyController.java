package cn.wolfcode.trip.app.controller;


import cn.wolfcode.trip.base.domain.Daily;
import cn.wolfcode.trip.base.domain.DailyComment;
import cn.wolfcode.trip.base.query.DailyCommentQueryObject;
import cn.wolfcode.trip.base.query.DailyQueryObject;
import cn.wolfcode.trip.base.service.IDailyCommentService;
import cn.wolfcode.trip.base.service.IDailyService;
import cn.wolfcode.trip.base.util.JsonResult;
import com.github.pagehelper.PageInfo;
import jdk.nashorn.internal.objects.annotations.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/dailies")
public class DailyController {

    @Autowired
    private IDailyService dailyService;
    @Autowired
    private IDailyCommentService commentService;
   @GetMapping("/{id}")
   public Daily getByid(@PathVariable Long id) {
       Daily daily = dailyService.get(id);
       return daily;
   }

    @GetMapping
    public PageInfo  listAll(DailyQueryObject qo){
        PageInfo query = dailyService.query(qo);
        return query;
   }

   @PostMapping("/{id}/visit")
    public JsonResult visitNum(Daily daily){
       dailyService.updateByVisitNum(daily);
       return new JsonResult();
   }
    /**
     * 查询日报的所有评论
     */
    @GetMapping("/comments/{dailyId}")
    public PageInfo listCommentByDailyId(DailyCommentQueryObject qo){
        PageInfo pageInfo = commentService.query(qo);
        return pageInfo;
    }
    /**
     * 添加一条评论
     */
    @PostMapping("/comments/{daily.id}")
    public JsonResult saveCommentByDaily(DailyComment comment){
        JsonResult jsonResult = new JsonResult();
        try {
            commentService.saveCommentByDaily(comment);
        } catch (Exception e) {
            e.printStackTrace();
            jsonResult.mark(e.getMessage());
        }
        return jsonResult;

    }
}
