package cn.wolfcode.trip.app.controller;

import cn.wolfcode.trip.base.domain.Daily;
import cn.wolfcode.trip.base.domain.LocationSituation;
import cn.wolfcode.trip.base.service.ILocationSituationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/LocationSituations")
public class LocationSituationController {

    @Autowired
    private ILocationSituationService locationSituationService;

    @GetMapping("/{id}")
    public LocationSituation getByid(@PathVariable Long id) {
        return locationSituationService.get(id);

    }

}
