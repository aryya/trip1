package cn.wolfcode.trip.app.controller;


import cn.wolfcode.trip.base.domain.ScenicSpotTicket;
import cn.wolfcode.trip.base.query.ScenicSpotTicketQueryObject;
import cn.wolfcode.trip.base.service.IScenicSpotTicketService;
import cn.wolfcode.trip.base.service.IScenicsService;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/scenics")
public class ScenicSpotTicketController {

    @Autowired
    private IScenicSpotTicketService scenicSpotTicketService;


    @GetMapping
    public PageInfo  listAll(ScenicSpotTicketQueryObject qo){
        PageInfo query = scenicSpotTicketService.query(qo);
        return query;
   }



}
