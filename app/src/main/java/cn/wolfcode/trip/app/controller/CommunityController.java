package cn.wolfcode.trip.app.controller;

import cn.wolfcode.trip.base.domain.Community;
import cn.wolfcode.trip.base.domain.CommunityReply;
import cn.wolfcode.trip.base.query.CommunityQueryObject;
import cn.wolfcode.trip.base.query.CommunityReplyQueryObject;
import cn.wolfcode.trip.base.query.QueryObject;
import cn.wolfcode.trip.base.service.ICommunityReplyService;
import cn.wolfcode.trip.base.service.ICommunityService;
import cn.wolfcode.trip.base.util.JsonResult;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 社区问答controller
 */
@RestController
@RequestMapping("/communities")
public class CommunityController {
    @Autowired
    private ICommunityService communityService;
    @Autowired
    private ICommunityReplyService replyService;
    /**
     * 查询所有的社区问题
     * @param qo
     * @return
     */
    @GetMapping
    public PageInfo listAllCommunities(CommunityQueryObject qo){
        qo.setOrderBy("c.creatTime desc");
        PageInfo pageInfo = communityService.listAllCommunities(qo);
        return pageInfo;
    }
    /**
     * 添加一条社区问题
     */
    @PostMapping
    public JsonResult saveProblem(Community community){
        communityService.saveProblem(community);
        return new JsonResult();
    }
    /**
     * 单条评论的详细内容
     */
    @GetMapping("/{id}")
    public Community getCommunityById(@PathVariable Long id){
        return communityService.getCommunityById(id);
    }
    /**
     * 获取问题的回复
     */
    @GetMapping("/replies/{communityId}")
    public PageInfo listAllReplyByCommunityId(CommunityReplyQueryObject qo){
        qo.setOrderBy("c.creatTime desc");
        PageInfo pageInfo = replyService.listAllReplyByCommunityId(qo);
        return pageInfo;
    }

    /**
     * 添加一条回复
     * @param communityReply
     * @return
     */
    @PostMapping("/addreplies")
    public JsonResult saveCommunityReply(CommunityReply communityReply){
        JsonResult jsonResult = new JsonResult();
        try {
            replyService.saveReply(communityReply);
        } catch (Exception e) {
            e.printStackTrace();
            jsonResult.mark(e.getMessage());
        }
        return jsonResult;
    }

    /**
     * 统计评论数
     */
    @GetMapping("/countReplise/{id}")
    public int countReplise(@PathVariable Long id){
        int count = replyService.countRepliseByCommunityId(id);
        return count;
    }
}
