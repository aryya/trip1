package cn.wolfcode.trip.app.controller;

import cn.wolfcode.trip.base.domain.*;
import cn.wolfcode.trip.base.query.ReplyQueryObject;
import cn.wolfcode.trip.base.query.StrategyCommentQueryObject;
import cn.wolfcode.trip.base.query.StrategyQueryObject;
import cn.wolfcode.trip.base.service.*;
import cn.wolfcode.trip.base.util.JsonResult;
import com.alibaba.druid.sql.ast.expr.SQLCaseExpr;
import com.github.pagehelper.PageInfo;
import net.sf.jsqlparser.expression.operators.relational.ItemsList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/strategies")
public class StrategyController {

    @Autowired
    private IStrategyService strategyService;
    @Autowired
    private IStrategyCatalogService catalogService;
    @Autowired
    private IStrategyDetailService detailService;
    @Autowired
    private IStrategyCommentService commentService;
    @Autowired
    private ICommentReplyService replyService;
    /**
     * 获取状态为推荐的所有攻略资源-app推荐
     *
     * @param qo
     * @return
     */
    @GetMapping
    public PageInfo query(StrategyQueryObject qo) {
        return strategyService.query(qo);
    }

    /**
     * 根据攻略id查询大攻略资源-title
     */
    @GetMapping("/{id}")
    public Strategy getById(@PathVariable Long id) {
        return strategyService.getById(id);
    }

    /**
     * 根据攻略id查询攻略catalog分类集合资源
     *
     * @return
     */
    @GetMapping("/{id}/catalogs")
    public List<StrategyCatalog> listByStrategyId(@PathVariable Long id) {
        return catalogService.listByStrategyId(id);

    }

    /**
     * 根据攻略文章id查询文章内容
     *
     * @return
     */
    @GetMapping("/details/{id}")
    public StrategyDetail getByDetailId(@PathVariable Long id) {
        return detailService.getByDetailId(id);
    }

    /**
     * 根据攻略id查询所有的评论
     *
     * @return pageInfo
     */
    @GetMapping("/{strategyId}/comments")
    public PageInfo listCommentByStrategyId(StrategyCommentQueryObject qo) {
        qo.setOrderBy("sc.createTime desc");
        PageInfo pageInfo = commentService.listCommentByStrategyId(qo);
        return pageInfo;
    }

    /**
     * 获取前六个标签
     * @param qo
     * @return
     */
    @GetMapping("/{strategyId}/tags")
    public PageInfo listTagsByStrategyId(StrategyCommentQueryObject qo) {
        qo.setPageSize(6);
        return commentService.listTagsByStrategyId(qo);
    }

    /**
     * 为指定的攻略新增点评信息
     */
    @PostMapping("/{strategy.id}/comments")
    public JsonResult save(StrategyComment strategyComment, String[] tags) {
        commentService.save(strategyComment,tags);
        return new JsonResult();
    }
    /**
     * 根据攻略评论id查评论信息
     */
    @GetMapping("/comments/{id}")
    public StrategyComment getCommentByCommentId(@PathVariable Long id){
        StrategyComment comment = commentService.getCommentByCommentId(id);
        return comment;
    }
    /**
     * 根据攻略评论id查询到所有的一级评论
     */
    @GetMapping("/commentReplies/{commentId}")
    public PageInfo listCommentReply(ReplyQueryObject qo){
        // 查询所有的一级评论
        qo.setOrderBy("c.carentTime desc");
        PageInfo pageInfo = replyService.listReplyByCommentId(qo);
        return pageInfo;

    }

    /**
     * 根据一级评论id查询到所有的二级评论
     */
    @GetMapping("/commentReplies/parent/{commentId}")
    public List<CommentReply> listByParentId(@PathVariable Long commentId){
        // 查询所有的二级评论
        List<CommentReply> list = replyService.listByParentId(commentId);
        return list;
    }
    /**
     * 保存一级评论
     */
    @PostMapping("/commentReplies")
    public JsonResult saveReply(CommentReply reply){
        replyService.save(reply);
        return new JsonResult();
    }
    /**
     * 查询评论数
     */
    @GetMapping("/countReplies/{commentId}")
    public int countReply(@PathVariable Long commentId){
        return replyService.selectCountReply(commentId);
    }
}
