package cn.wolfcode.trip.app.controller;

import cn.wolfcode.trip.base.domain.Chat;
import cn.wolfcode.trip.base.domain.User;
import cn.wolfcode.trip.base.query.ChatQueryObject;
import cn.wolfcode.trip.base.service.IChatService;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/chat")
public class ChatController {

    @Autowired
    private IChatService chatService;

    @GetMapping("/getNewUnReadMsg")
    public List getNewUnReadMsg(ChatQueryObject qo){
        List list = chatService.getNewUnReadMsg(qo);
        chatService.updateChatState(list);
        return list;
    }

    @PostMapping("/insertChatMsg")
    public Chat createNewChat(Chat chat){
        return chatService.createNewChat(chat);
    }

    /**
     * 获取私信聊天的历史
     * @param qo
     * @return
     */
    @GetMapping("/chatHistory")
    public PageInfo getChatHistory(ChatQueryObject qo){
        qo.setPageSize(0);
        qo.setOrderBy("msgDate");
         PageInfo pageInfo= chatService.getChatHistory(qo);
        chatService.updateChatState(pageInfo.getList());
         return pageInfo;
    }

    /**
     * 获取当前用户有私信聊天过的对象
     * @param qo
     * @return
     */
    @GetMapping("/receivers/{currentUserId}")
    public PageInfo<User> ListReceivers(ChatQueryObject qo){
        PageInfo pageInfo= chatService.ListReceivers(qo);
        return pageInfo;
    }

    /**
     * 通过当前用户id查询未读的信息
     *
     * @param userId 当前用户的id
     * @return 未读的消息数
     */
    @GetMapping("/unReadMsgNum/{userId}")
    public int getUnReadMsgNum(@PathVariable Integer userId) {
        return chatService.getUnReadMsgNum(userId);
    }

}
