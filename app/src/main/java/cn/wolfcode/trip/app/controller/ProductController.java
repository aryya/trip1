package cn.wolfcode.trip.app.controller;

import cn.wolfcode.trip.base.query.ProductQueryObject;
import cn.wolfcode.trip.base.service.IProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

//商品资源控制器
@RestController
@RequestMapping("/products")
public class ProductController {

    @Autowired
    private IProductService productService;

    /**
     * 1.获取商品列表
     * 资源：/products
     * 动作：GET
     * 参数：分页
     */
    @GetMapping
    public Object queryProduct(ProductQueryObject qo) {
        return productService.query(qo);
    }

    /**
     * 2.获取某个商品的信息
     * 资源：/products/{productId}
     * 动作：GET
     * 参数：商品id：productId
     */
    @GetMapping("/{productId}")
    public Object getProduct(@PathVariable Long productId) {
        return productService.get(productId);
    }
}
