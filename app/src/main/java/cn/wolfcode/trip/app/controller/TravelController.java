package cn.wolfcode.trip.app.controller;

import cn.wolfcode.trip.base.domain.Travel;
import cn.wolfcode.trip.base.domain.TravelComment;
import cn.wolfcode.trip.base.domain.User;
import cn.wolfcode.trip.base.query.QueryObject;
import cn.wolfcode.trip.base.query.TravelCommendQueryObject;
import cn.wolfcode.trip.base.query.TravelCommentQueryObject;
import cn.wolfcode.trip.base.query.TravelQueryObject;
import cn.wolfcode.trip.base.service.ITravelCommendService;
import cn.wolfcode.trip.base.service.ITravelCommentService;
import cn.wolfcode.trip.base.service.ITravelService;
import cn.wolfcode.trip.base.service.IUserService;
import cn.wolfcode.trip.base.util.JsonResult;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/travels")
public class TravelController {

    @Autowired
    private ITravelService travelService;

    @Autowired
    private ITravelCommendService commendService;
    @Autowired
    private ITravelCommentService commentService;

    @PostMapping
    public JsonResult save(Travel travel) {
        if (travel.getId() != null) {
            travelService.updateByPrimaryKey(travel);

        } else {
            travelService.insert(travel);
        }
        return new JsonResult();
    }

    /**
     * 更新数据要带上id
     *
     * @param travel
     * @return
     */
    @PutMapping("/{id}")
    public JsonResult update(Travel travel) {
        travelService.updateByPrimaryKey(travel);
        return new JsonResult();
    }

    /**
     * app编辑页面和查看游记回显数据, 需要带上参数id
     * PathVariable 获取url的值
     *
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public Travel getById(@PathVariable Long id) {
        return travelService.getById(id);
    }

    /**
     * 查询所有的游记显示在app页面上
     *请求方式:GET
     * @return 分页
     */
    @GetMapping
    public PageInfo query(TravelQueryObject qo) {
        return travelService.queryTravel(qo);
    }

    /**
     * 返回pageInfo, 方便页面渲染时传入数据
     * @param qo
     * @return
     */
    @GetMapping("/commends")
    public PageInfo listCommends(TravelCommendQueryObject qo){
        qo.setOrderBy("tc.schedule desc");
        return commendService.queryForApp(qo);
    }

    /**
     * 查询游记的评论
     * @param qo
     * @return
     */
    @GetMapping("/comments/{travelId}")
    public PageInfo getCommentByTravelId(TravelCommentQueryObject qo){
        qo.setOrderBy("tc.carentTime desc");
        PageInfo pageInfo = commentService.getCommentByTravelId(qo);
        return pageInfo;
    }
    /**
     * 新增游记评论
     */
    @PostMapping("/comments/{travel.id}")
    public JsonResult saveTravelComment(TravelComment comment){
        JsonResult jsonResult = new JsonResult();
        try {
            commentService.save(comment);
        } catch (Exception e) {
            e.printStackTrace();
            jsonResult.mark(e.getMessage());
        }
        return jsonResult;
    }
    /**
     * 查询个人页面中的游记文章及总数量
     * @param id
     * @param qo
     * @return
     */
    @GetMapping("/{id}/users")
    public PageInfo listContentByUserId(@PathVariable Long id, QueryObject qo) {
        return travelService.listContentByUserId(id, qo);
    }

}
