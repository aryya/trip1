package cn.wolfcode.trip.app.controller;

import cn.wolfcode.trip.base.domain.Follow;
import cn.wolfcode.trip.base.domain.OrderInfo;
import cn.wolfcode.trip.base.domain.User;
import cn.wolfcode.trip.base.query.QueryObject;
import cn.wolfcode.trip.base.query.OrderInfoQueryObject;
import cn.wolfcode.trip.base.query.QueryObject;
import cn.wolfcode.trip.base.query.TravelQueryObject;
import cn.wolfcode.trip.base.query.UserCommentsQueryObject;
import cn.wolfcode.trip.base.query.UserQueryObject;
import cn.wolfcode.trip.base.query.UserQueryObject;
import cn.wolfcode.trip.base.service.ITravelService;
import cn.wolfcode.trip.base.service.IUserLikeService;
import cn.wolfcode.trip.base.service.IUserService;
import cn.wolfcode.trip.base.service.*;
import cn.wolfcode.trip.base.util.JsonResult;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;


import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/users")
public class UserController {

    // 我的收藏
    @Autowired
    private IUserLikeService iUserLikeService;

    @Autowired
    private IUserService userService;
    //注入游记


    @Autowired
    private ITravelService travelService;

    /**
     * 根据用户的id查询对应的数据
     * @param id 要查询的用户id
     * @return 查询到的用户id
     */
    @GetMapping("/getUserById")
    public User getUserById(Long id){
        return userService.selectByPrimaryKey(id);
    }
    @Autowired
    private IFinanceService financeService;

    @Autowired
    private IUserAddressService userAddressService;

    @Autowired
    private IOrderInfoService orderInfoService;

    @Autowired
    private ISaleRecordService saleRecordService;


    /**
     * 查询用户所有消费情况
     * 资源；/users/{userId}/records
     * 动作：GET
     * 参数：用户id
     */
    @GetMapping("/{userId}/records")
    public Object querySaleRecords(QueryObject qo) {
        return saleRecordService.query(qo);

    }


    /**
     * 查询某个用户的所有订单
     * 资源：/users/{userId}/orders/{status}
     * 动作：GET
     * 参数：userId ,status
     */
    @GetMapping("/{userId}/orders/{status}")
    public Object queryOrders(OrderInfoQueryObject qo) {
        return orderInfoService.query(qo);

    }


    /**
     * 保存用户订单
     * 资源：/users/{userId}/orders
     * 动作：POST
     * 参数：用户id 订单对象，订单数量
     */
    @PostMapping("/{userId}/orders")
    public Object saveOrder(@PathVariable Long userId, OrderInfo entity, Integer buyNum) {
        JsonResult result = new JsonResult();
        //为订单添加收件人
        entity.setUserId(userId);
        try {
            orderInfoService.save(entity, buyNum);
        } catch (Exception e) {
            e.printStackTrace();
            result.mark(e.getMessage());
        }
        return result;
    }


    /**
     * 删除用户的某个收货地址
     * 资源：/users/{userId}/addresses/{addressId}
     * 动作：DELETE
     * 参数：用户id和收货地址
     */
    @DeleteMapping("/{userId}/addresses/{addressId}")
    public void deleteUserAddresses(@PathVariable Long addressId, HttpServletResponse response) {
        userAddressService.delete(addressId);
        //设置响应状态 204是删除
        response.setStatus(HttpServletResponse.SC_NO_CONTENT);
    }


    /**
     * 编辑某个用户的某个收货地址（设置是否是默认收货地址）
     * 资源：/users/{userId}/addresses/{addressId}
     * 动作：PUT
     * 参数：用户的id 和收货地址id
     */
    @PutMapping("/{userId}/addresses/{addressId}")
    public Object getUserAddresses(@PathVariable Long userId, @PathVariable Long addressId) {
        JsonResult result = new JsonResult();
        try {
            userAddressService.updateDefaultAddress(userId, addressId);
        } catch (Exception e) {
            result.mark(e.getMessage());
        }
        return result;
    }


    /**
     * 获取某个用户的所有收货地址
     * 资源：/users/{userId}/addresses
     * 动作：GET
     * 参数：用户的id
     */
    @GetMapping("/{userId}/addresses")
    public Object getUserAddresses(@PathVariable Long userId) {
        return userAddressService.listAll(userId);
    }


    /**
     * 获取某个用户的积分和金钱
     * 资源：/users/{userId}/finance
     * 动作：GET
     * 参数：用户的id
     */

    @GetMapping("/{userId}/finance")
    public Object getUserFinance(@PathVariable Long userId) {
        return financeService.getUserFinance(userId);
    }


    //前台发送注册信息给后台查询验证返回数据
    @PostMapping
    public JsonResult register(User user) {
        JsonResult jsonResult = new JsonResult();

        try {
            userService.register(user);
        } catch (Exception e) {
            e.printStackTrace();
            jsonResult.mark(e.getMessage());
        }
        return jsonResult;
    }

    //列出指定id用户的信息-(个人页面userProfiles.html)
    @GetMapping("/{authorId}")
    public User listUsers(@PathVariable Long authorId) {
        return userService.selectByPrimaryKey(authorId);
    }

    /**
     * 更新用户信息
     * @param user
     * @return
     */
    @PutMapping("/{id}")
    public JsonResult update(User user) {
        JsonResult jsonResult = new JsonResult();
        userService.updateByPrimaryKey(user);
        jsonResult.setResult(user);
        return jsonResult;
    }

    /**
     * 查询游记
     * @return pageinfo
     */
    @GetMapping("/{authorId}/travels")
    public PageInfo queryTravel(TravelQueryObject qo){

        qo.setOrderBy("lastUpdateTime DESC");
        return travelService.queryTravel(qo);
    }
    /**
     * 查询用户的评论
     */
    @GetMapping("/comments")
    public List listCommentsByUserId(UserCommentsQueryObject qo){
        List list = userService.listCommentsByUserId(qo);
        return list;
    }

    /**
     *
     * @param qo
     * @return
     */
    @GetMapping("/travelsComments")
    public List listTravelsCommentsByUserId(UserCommentsQueryObject qo){
        List list = userService.listCommentsByUserId(qo);
        return list;
    }
    /**
     * 关注:查询登录用户id和文章作者id是否存在
     *
     * @param userId
     * @param AuthorId
     * @return
     */
    @GetMapping("/{userId}/{AuthorId}")
    public Boolean listMutualRelationship(@PathVariable("userId") Long userId, @PathVariable("AuthorId") Long AuthorId) {
        Follow follow = userService.listMutualRelationship(userId, AuthorId);
        if (follow != null) {
            return true;
        }
        return false;
    }

    /**
     * 新增互相关注的id信息
     *
     * @param userId
     * @param AuthorId
     * @return
     */
    @PostMapping("/{userId}/{AuthorId}")
    public JsonResult saveMutualRelationship(@PathVariable("userId") Long userId, @PathVariable("AuthorId") Long AuthorId) {

        userService.saveMutualRelationship(userId, AuthorId);
        return new JsonResult();
    }

    /**
     * 删除互相关注的id信息
     *
     * @param userId
     * @param AuthorId
     */
    @DeleteMapping("/{userId}/{AuthorId}")
    public boolean deleteMutualRelationship(@PathVariable("userId") Long userId, @PathVariable("AuthorId") Long AuthorId) {
        userService.deleteMutualRelationship(userId, AuthorId);
        return true;
    }

    /**
     * 个人页面查询关注的人数
     *
     * @return
     */
    @GetMapping("/follows/{userId}")
    public int listFollowNum(@PathVariable Long userId) {
        return userService.listFollowNum(userId);

    }

    /**
     * 个人页面查询粉丝的人数
     *
     * @return
     */
    @GetMapping("/followers/{authorId}")
    public int listFollowerNum(@PathVariable Long authorId) {
        return userService.listFollowerNum(authorId);
    }

   /**
     * 个人页面查询指定用户所有的评论
     * @param qo
     * @return
     */
    @GetMapping("/{userId}/comments")
    public PageInfo queryCommetsByUserId(UserQueryObject qo) {

        qo.setOrderBy("createTime DESC");
        return userService.queryCommetsByUserId(qo);
    }

    /**
     * 个人页面查询所有关注的人
     * @param qo
     * @return
     */
    @GetMapping("/{userId}/follows")
    public PageInfo listFollowsById(UserQueryObject qo){
        return userService.listFollowsById(qo);
    }

    //收藏首页查询
    @GetMapping("/userlikes/{userId}")
    public List selectByUerId(UserQueryObject qo){
        return iUserLikeService.selectByUserId(qo);
    }

    // 游记,攻略,日报查询
    @GetMapping("/{userId}/userlikes")
    public List<Map<String ,Object>> selctAll(@PathVariable Long userId){
        return iUserLikeService.selectAll(userId);
    }


    // 查询所有的游记,攻略,日报的封面,标题;
    @GetMapping("/strategy")
    public List getStrategys(UserQueryObject qo,String keyword){
        qo.setKeyword(keyword);
        return iUserLikeService.getStrategys(qo);
    }
    @GetMapping("/travel")
    public List getTravel(UserQueryObject qo,String keyword){
        qo.setKeyword(keyword);
        return iUserLikeService.getTravel(qo);
    }
    @GetMapping("/daily")
    public List getDaily(UserQueryObject qo,String keyword){
        qo.setKeyword(keyword);
        return iUserLikeService.getDaily(qo);
    }





    /**
     * 获取头像
     */
    @GetMapping("/headImgUrl/{userId}")
    public String getUserHeadImgUrl(@PathVariable Long userId){
        String url = userService.getUserHeadImgUrl(userId);
        return url;
    }
}
