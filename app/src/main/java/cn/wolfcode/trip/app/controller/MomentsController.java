package cn.wolfcode.trip.app.controller;

import cn.wolfcode.trip.base.domain.Moments;
import cn.wolfcode.trip.base.domain.MomentsComment;
import cn.wolfcode.trip.base.query.MomentsCommentQueryObject;
import cn.wolfcode.trip.base.query.MomentsQueryObject;
import cn.wolfcode.trip.base.service.IMomentsService;
import cn.wolfcode.trip.base.util.JsonResult;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/moments")
public class MomentsController {

    @Autowired
    private IMomentsService momentsService;

    /**
     * 查询出朋友圈的全部动态
     *
     * @param qo
     * @return
     */
    @GetMapping("/{userId}")
    public PageInfo queryMomentsByUserId(MomentsQueryObject qo) {
        qo.setOrderBy("createTime DESC");
        PageInfo pageInfo = momentsService.queryMomentsByUserId(qo);
        return pageInfo;
    }

    /**
     * 新增朋友圈的动态
     *
     * @param moments
     * @return
     */
    @PostMapping("/{userId}/comments")
    public JsonResult save(Moments moments) {
        momentsService.save(moments);
        return new JsonResult();
    }

    /*
     * 查询朋友圈每条动态所有的评论
     * @param qo
     * @return pageInfo
     */
    @GetMapping("/{momentsId}/comments")
    public PageInfo queryCommentsById(MomentsCommentQueryObject qo) {
        qo.setPageSize(10);
        qo.setOrderBy("createTime DESC");
        return momentsService.queryCommentsById(qo);
    }

    /**
     * 新增:评论内容
     *
     * @param momentsComment
     * @return
     */
    @PostMapping("/review")
    public JsonResult saveComments(MomentsComment momentsComment) {
        momentsService.saveComments(momentsComment);
        return new JsonResult();
    }




}
