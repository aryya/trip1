package cn.wolfcode.trip.base.service.impl;

import cn.wolfcode.trip.base.domain.Sign;
import cn.wolfcode.trip.base.mapper.SignMapper;
import cn.wolfcode.trip.base.query.SignQueryObject;
import cn.wolfcode.trip.base.service.ISignService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Service
public class ISignServiceImpl implements ISignService {
    @Autowired
    public SignMapper signMapper;



    public List getMonthSign(SignQueryObject qo) {
        return signMapper.selectMonthSign(qo);
    }

    public Sign creatNewSign(SignQueryObject qo) {
        //获取前一天的时间
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_MONTH,-1);
        Date date = calendar.getTime();

        Sign preSign= signMapper.getPreSignByTime(date);

        Sign sign = new Sign();
        sign.setSignDate(new Date());
        sign.setUserId(qo.getCurrentUserId());
        if(preSign!=null){
            sign.setDays(preSign.getDays()+1);
        }
        signMapper.insert(sign);
        return sign;
    }


    public void addIntegral(Long currentUserId, Integer integral) {

    }
}
