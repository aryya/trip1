package cn.wolfcode.trip.base.service;

import cn.wolfcode.trip.base.domain.Product;
import cn.wolfcode.trip.base.query.ProductQueryObject;
import com.github.pagehelper.PageInfo;

import java.util.List;

public interface IProductService {

    /**
     * 保存商品
     * @param entity
     */
    void save(Product entity);

    /**
     * 删除
     * @param id
     */
    void delete(Long id);

    /**
     * 更新
     * @param entity
     */
    void update(Product entity);

    /**
     * 获取单个商品
     * @param id
     * @return
     */
    Product get(Long id);

    /**
     * 商品列表
     * @return
     */
    List<Product> listAll();

    /**
     * 商品列表分页
     * @param qo
     * @return
     */
    PageInfo<Product> query(ProductQueryObject qo);
}
