package cn.wolfcode.trip.base.domain;

import cn.wolfcode.trip.base.util.JsonResult;
import cn.wolfcode.trip.base.util.JsonUtil;
import lombok.*;

import java.util.HashMap;
import java.util.Map;

/**
 * 注册用户
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Region extends BaseDomain {

    public static final Integer STATUS_NORMAL = 0;//普通
    public static final Integer STATUS_HOT = 1;//热门

    //地区名
    private String name;
    //主id
    private Region parent;
    //状态
    private Integer state;

    //封装地区的数据,前端从这里取值
    public Map ToTreeMap() {
        HashMap<String, Object> map = new HashMap();
        map.put("id", this.id);
        map.put("text", this.name);
        map.put("lazyLoad", true);
        //状态为热门, 设置tags为推荐
        if (this.state == STATUS_HOT) {
            map.put("tags", new String[]{"推荐"});
        }
        return map;
    }

    public String getJson() {
        HashMap<Object, Object> map = new HashMap();
        map.put("id", this.id);
        map.put("name", this.name);
        map.put("state", this.state);

        if (parent != null) {
            map.put("parentId", parent.getId());
            map.put("parentName", parent.getName());
        }
        return JsonUtil.getJsonString(map);
    }

}