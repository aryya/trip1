package cn.wolfcode.trip.base.service;

import cn.wolfcode.trip.base.domain.CommentReply;
import cn.wolfcode.trip.base.query.ReplyQueryObject;
import com.github.pagehelper.PageInfo;

import java.util.List;

public interface ICommentReplyService {
    // 查询所有一级评论
    PageInfo listReplyByCommentId(ReplyQueryObject qo);
    // 查询所有的二级评论
    List<CommentReply> listByParentId(Long parentId);
    // 保存一级评论
    void save(CommentReply reply);
    // 统计评论总数
    int selectCountReply(Long commentId);
    // 统计所有的未读评论数据
    List<CommentReply> listStrategyCommentByUserId(Long userId);
}
