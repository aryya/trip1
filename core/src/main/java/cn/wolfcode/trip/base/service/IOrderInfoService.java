package cn.wolfcode.trip.base.service;

import cn.wolfcode.trip.base.domain.OrderInfo;
import cn.wolfcode.trip.base.query.OrderInfoQueryObject;
import com.github.pagehelper.PageInfo;

import java.util.List;

public interface IOrderInfoService {

    /**
     * 保存订单
     * @param entity
     * @param buyNum 购买数量
     */
    void save(OrderInfo entity, Integer buyNum);

    /**
     * 删除订单
     * @param id
     */
    void delete(Long id);

    /**
     * 更新订单
     * @param entity
     */
    void update(OrderInfo entity);

    /**
     * 获取单个订单
     * @param id
     * @return
     */
    OrderInfo get(Long id);

    /**
     * 查询全部订单
     * @return
     */
    List<OrderInfo> listAll();

    /**
     * 订单列表分页
     * @param qo
     * @return
     */
    PageInfo<OrderInfo> query(OrderInfoQueryObject qo);


}
