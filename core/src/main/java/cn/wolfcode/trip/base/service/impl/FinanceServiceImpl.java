package cn.wolfcode.trip.base.service.impl;

import cn.wolfcode.trip.base.domain.Finance;
import cn.wolfcode.trip.base.mapper.FinanceMapper;
import cn.wolfcode.trip.base.service.IFinanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

//用户财产接口
@Service
public class FinanceServiceImpl implements IFinanceService {

    @Autowired
    private FinanceMapper financeMapper;

    public Finance getUserFinance(Long userId) {
        return financeMapper.getFinanceByUserId(userId);
    }
}
