package cn.wolfcode.trip.base.mapper;

import cn.wolfcode.trip.base.domain.MomentsComment;
import cn.wolfcode.trip.base.query.MomentsCommentQueryObject;
import java.util.List;

public interface MomentsCommentMapper {
    int deleteByPrimaryKey(Long id);

    int insert(MomentsComment record);

    MomentsComment selectByPrimaryKey(Long id);

    List<MomentsComment> selectAll();

    int updateByPrimaryKey(MomentsComment record);

    List selectCommentsById(MomentsCommentQueryObject qo);
}