package cn.wolfcode.trip.base.domain;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

//用户财产
@Setter
@Getter
public class Finance extends BaseDomain {

    private BigDecimal integral; //积分

    private BigDecimal money; //金钱


}