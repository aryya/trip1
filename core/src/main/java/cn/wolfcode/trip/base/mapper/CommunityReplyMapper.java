package cn.wolfcode.trip.base.mapper;

import cn.wolfcode.trip.base.domain.CommunityReply;
import cn.wolfcode.trip.base.query.CommunityReplyQueryObject;
import cn.wolfcode.trip.base.query.QueryObject;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CommunityReplyMapper {
    int deleteByPrimaryKey(Long id);

    int insert(CommunityReply record);

    CommunityReply selectByPrimaryKey(Long id);

    List<CommunityReply> selectAll();

    int updateByPrimaryKey(CommunityReply record);

    List listAllReplyByCommunityId(CommunityReplyQueryObject qo);

    int countRepliseByCommunityId(@Param("id") Long id);
}