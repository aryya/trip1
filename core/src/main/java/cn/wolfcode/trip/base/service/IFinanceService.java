package cn.wolfcode.trip.base.service;

import cn.wolfcode.trip.base.domain.Finance;

public interface IFinanceService {
    /**
     * 获取用户的积分和金钱
     * @param userId 用户的id
     * @return 返回用户积分和金钱
     */
    Finance getUserFinance(Long userId);
}
