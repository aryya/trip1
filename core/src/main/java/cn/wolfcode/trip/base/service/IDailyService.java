package cn.wolfcode.trip.base.service;

import cn.wolfcode.trip.base.domain.Daily;
import cn.wolfcode.trip.base.query.DailyQueryObject;
import com.github.pagehelper.PageInfo;

public interface IDailyService {
    /**
     * 日报分页
     * @param qo
     * @return
     */
    PageInfo query(DailyQueryObject qo);

    Daily get(Long id);

    /**
     * /访问点击
     * @param daily
     */
    void updateByVisitNum(Daily daily);

    /**
     * 编辑,修改
     * @param daily
     */
    void saveOrUpdate(Daily daily);
}
