package cn.wolfcode.trip.base.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * 社区问答的问题类
 */
@Setter
@Getter
public class Community extends BaseDomain{
    public static final Integer STATUS_NORMAL = 0;//普通
    public static final Integer STATUS_HOT = 1;//热门

    // 提问者
    private User user;
    // 标题
    private String title;
    // 具体问题
    private String content;
    // 图片s
    private String imgUrls;
    // 创建时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date creatTime;
    // 发布状态
    private Integer state;

}