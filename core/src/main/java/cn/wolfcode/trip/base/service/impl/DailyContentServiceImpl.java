package cn.wolfcode.trip.base.service.impl;

import cn.wolfcode.trip.base.domain.Daily;
import cn.wolfcode.trip.base.domain.DailyContent;
import cn.wolfcode.trip.base.domain.StrategyContent;
import cn.wolfcode.trip.base.domain.StrategyDetail;
import cn.wolfcode.trip.base.mapper.DailyContentMapper;
import cn.wolfcode.trip.base.mapper.DailyMapper;
import cn.wolfcode.trip.base.query.DailyQueryObject;
import cn.wolfcode.trip.base.service.IDailyContentService;
import cn.wolfcode.trip.base.service.IDailyService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DailyContentServiceImpl implements IDailyContentService {

    @Autowired
    private DailyContentMapper dailyContentMapper;


    public DailyContent getDailyContent(Long id) {
        return dailyContentMapper.selectByPrimaryKey(id);
    }


}
