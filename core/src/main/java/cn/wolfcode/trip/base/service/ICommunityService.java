package cn.wolfcode.trip.base.service;

import cn.wolfcode.trip.base.domain.Community;
import cn.wolfcode.trip.base.query.CommunityQueryObject;
import cn.wolfcode.trip.base.query.QueryObject;
import com.github.pagehelper.PageInfo;

public interface ICommunityService {
    // 查询所有的问题
    PageInfo listAllCommunities(CommunityQueryObject qo);
    // 添加一条问题
    void saveProblem(Community community);
    // 查询单条问题
    Community getCommunityById(Long id);
}
