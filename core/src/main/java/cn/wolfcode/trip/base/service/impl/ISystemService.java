package cn.wolfcode.trip.base.service.impl;

import cn.wolfcode.trip.base.domain.SystemMsg;

import java.util.List;

public interface ISystemService {

    /**
     * 根据当前用户的id查询所有的系统消息
     * @param currentId 当前用户的id
     * @return
     */
    List<SystemMsg> getAllSytemMsgByCurrentId(Long currentId);

    /**
     * 修改系统消息的状态
     * @param systemMsg 系统消息
     */
    void updateState(SystemMsg systemMsg);

    /**
     *  查询当前用户未读的系统消息
     * @param currentId
     * @return
     */
    int getSystemMsgCount(Long currentId);
}
