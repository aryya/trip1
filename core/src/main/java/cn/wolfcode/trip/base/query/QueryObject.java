package cn.wolfcode.trip.base.query;

import lombok.Getter;
import lombok.Setter;
import org.springframework.util.StringUtils;

@Getter
@Setter
public class QueryObject {
    private int currentPage = 1;//当前页
    private int pageSize = 3;//每页显示多少行
    private String orderBy;//排序功能

    public int getStart() {
        return (currentPage - 1) * pageSize;
    }


    public String getOrderBy() {
        return StringUtils.hasLength(orderBy) ? orderBy : null;
    }
}
