package cn.wolfcode.trip.base.mapper;

import cn.wolfcode.trip.base.domain.Product;
import cn.wolfcode.trip.base.query.ProductQueryObject;

import java.util.List;

public interface ProductMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Product record);

    Product selectByPrimaryKey(Long id);

    List<Product> selectAll();

    int updateByPrimaryKey(Product record);

    List<Product> selectForList(ProductQueryObject qo);
}