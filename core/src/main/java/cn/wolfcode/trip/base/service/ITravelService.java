package cn.wolfcode.trip.base.service;

import cn.wolfcode.trip.base.domain.Travel;
import cn.wolfcode.trip.base.domain.TravelContent;
import cn.wolfcode.trip.base.query.QueryObject;
import cn.wolfcode.trip.base.query.TravelQueryObject;
import com.github.pagehelper.PageInfo;

import java.util.List;

public interface ITravelService {

    int deleteByPrimaryKey(Long id);

    int insert(Travel record);

    Travel selectByPrimaryKey(Long id);

    List<Travel> selectAll();

    int updateByPrimaryKey(Travel record);

    PageInfo queryTravel(TravelQueryObject qo);

    Travel getById(Long id);

    TravelContent getContentById(Long id);

    void changeState(Travel travel);

    PageInfo listContentByUserId(Long id, QueryObject qo);
}
