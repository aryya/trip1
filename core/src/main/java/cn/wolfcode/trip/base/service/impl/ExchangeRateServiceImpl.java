package cn.wolfcode.trip.base.service.impl;

import cn.wolfcode.trip.base.domain.ExchangeRate;
import cn.wolfcode.trip.base.mapper.ExchangeRateMapper;
import cn.wolfcode.trip.base.query.QueryObject;
import cn.wolfcode.trip.base.service.IExchangeRateService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

@Service
public class ExchangeRateServiceImpl implements IExchangeRateService {
    @Autowired
    private ExchangeRateMapper rateMapper;

    /**
     * 查询所有的货币
     * @param qo
     * @return
     */
    public PageInfo listAll(QueryObject qo) {
        PageHelper.startPage(qo.getCurrentPage(),qo.getPageSize());
        List<ExchangeRate> list = rateMapper.selectAll();
        return new PageInfo(list);
    }

    /**
     * 查询汇率
     * @param id
     * @return
     */
    public BigDecimal selectInSidePriceById(Long id) {
        return rateMapper.selectInSidePriceById(id);
    }

    /**
     * 计算汇率
     * @param exchangeRateId
     * @param money
     * @return
     */
    public BigDecimal countCountRates(Long exchangeRateId, BigDecimal money) {
        BigDecimal decimal = rateMapper.selectInSidePriceById(exchangeRateId);
        if(exchangeRateId==1||exchangeRateId==5||exchangeRateId==6){
            BigDecimal multiply = money.multiply(new BigDecimal("100"));
            BigDecimal divide = multiply.divide(decimal,4, BigDecimal.ROUND_HALF_UP);
            return divide;
        }else {
            BigDecimal divide = money.divide(decimal, 4, BigDecimal.ROUND_HALF_UP);
            return divide.multiply(new BigDecimal("100"));
        }
    }

    /**
     * 查询缩写名字
     * @return
     */
    public String selectRatesMinName(Long exchangeRateId){
        return rateMapper.selectRatesMinName(exchangeRateId);
    }
}
