package cn.wolfcode.trip.base.service;


import cn.wolfcode.trip.base.domain.SaleRecord;
import cn.wolfcode.trip.base.query.QueryObject;
import com.github.pagehelper.PageInfo;

import java.util.List;

public interface ISaleRecordService {


    /**
     * 保存消费记录
     *
     * @param entity
     */
    void save(SaleRecord entity);

    /**
     * 删除消费记录
     *
     * @param id
     */
    void delete(Long id);

    /**
     * 更新消费记录
     *
     * @param entity
     */
    void update(SaleRecord entity);

    /**
     * 获取单个消费记录
     *
     * @param id
     * @return
     */
    SaleRecord get(Long id);

    /**
     * 查询全部消费记录
     *
     * @return
     */
    List<SaleRecord> listAll();

    /**
     * 消费记录列表分页
     *
     * @param qo
     * @return
     */
    PageInfo<SaleRecord> query(QueryObject qo);


}
