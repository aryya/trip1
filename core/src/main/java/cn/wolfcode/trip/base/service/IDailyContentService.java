package cn.wolfcode.trip.base.service;

import cn.wolfcode.trip.base.domain.Daily;
import cn.wolfcode.trip.base.domain.DailyContent;
import cn.wolfcode.trip.base.domain.StrategyContent;
import cn.wolfcode.trip.base.domain.StrategyDetail;
import cn.wolfcode.trip.base.query.DailyQueryObject;
import com.github.pagehelper.PageInfo;

public interface IDailyContentService {

    DailyContent getDailyContent(Long id);

}
