package cn.wolfcode.trip.base.util;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class JsonResult {

    /**
     * 创建JsonResult类,方便传递参数
     */
    private boolean success = true;
    private String msg;
    //
    private Object result;

    //定义一个方法,方便调用该方法来封装数据
    public void mark(String msg) {
        this.success = false;
        this.msg = msg;
    }
}
