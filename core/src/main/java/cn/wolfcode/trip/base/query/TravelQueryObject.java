package cn.wolfcode.trip.base.query;

import lombok.Getter;
import lombok.Setter;
import org.springframework.util.StringUtils;

@Getter
@Setter
public class TravelQueryObject extends QueryObject {

    //高级查询作者的id
    private Long authorId;

    //查询关键字
    private String keyword;

    //查询审核状态
    private Integer state;

    //筛选掉不公开的
    private Boolean isPublic;

    private Long travelId;

    public String getKeyword() {
        return StringUtils.hasLength(keyword) ? keyword.trim() : null;
    }


}
