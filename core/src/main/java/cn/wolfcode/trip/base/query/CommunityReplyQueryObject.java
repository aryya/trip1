package cn.wolfcode.trip.base.query;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class CommunityReplyQueryObject extends QueryObject{
    private Long communityId;
}
