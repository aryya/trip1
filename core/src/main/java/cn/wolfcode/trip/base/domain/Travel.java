package cn.wolfcode.trip.base.domain;

import cn.wolfcode.trip.base.util.JsonUtil;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Getter
@Setter
public class Travel extends BaseDomain {

    public static final int STATE_DRAFT = 0;//草稿
    public static final int STATE_AUDIT = 1;//待审核
    public static final int STATE_POST = 2;//已发布
    public static final int STATE_REJECT = -1;//拒绝

    //标题
    private String title;
    //旅游时间
    //前端发请求中的日期转换格式给后台,不转换的话就会报500格式异常
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date travelTime;
    //人均消费
    private String perExpends;
    //旅游天数
    private Integer days;
    //和谁旅游
    private Integer person;
    //作者
    private User author;
    //创建时间
    private Date createTime;
    //发布时间
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date releaseTime;
    //是否公开
    private Boolean isPublic;
    //旅游地区
    private Region place;
    //封面
    private String coverUrl;
    //最后更新时间
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GTM+8")//后台转格式回显给前台
    private Date lastUpdateTime;
    //状态, 给个默认值
    private Integer state = STATE_DRAFT;
    //游记内容
    private TravelContent travelContent;

    //参与人物
    public String getPersonName() {
        String temp = "";
        switch (state) {
            case 1:
                temp = "一个人的旅行";
                break;
            case 2:
                temp = "和父母";
                break;
            case 3:
                temp = "和朋友";
                break;
            case 4:
                temp = "和同事";
                break;
            case 5:
                temp = "和爱人";
                break;
            case 6:
                temp = "和其他";
                break;
        }
        return temp;
    }

    //状态判断,switch需要常量int表示,不能用包装类型
    public String getStateName() {
        String temp = "";
        switch (state) {
            case STATE_AUDIT:
                temp = "待审核";
                break;
            case STATE_POST:
                temp = "已发布";
                break;
            case STATE_REJECT:
                temp = "拒绝";
                break;
            default:
                temp = "草稿";
        }
        return temp;
    }

    public String getJsonData() {
        HashMap<Object, Object> map = new HashMap();
        map.put("id", id);
        map.put("title", title);
        map.put("coverUrl", coverUrl);
        return JsonUtil.getJsonString(map);
    }

}