package cn.wolfcode.trip.base.service.impl;

import cn.wolfcode.trip.base.domain.Daily;
import cn.wolfcode.trip.base.mapper.DailyMapper;
import cn.wolfcode.trip.base.query.DailyQueryObject;
import cn.wolfcode.trip.base.query.QueryObject;
import cn.wolfcode.trip.base.service.IDailyService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class DailyServiceImpl implements IDailyService {

    @Autowired
    private DailyMapper dailyMapper;



    public PageInfo query(DailyQueryObject qo) {
        PageHelper.startPage(qo.getCurrentPage(),qo.getPageSize());
        List<Daily> list= dailyMapper.selectForList(qo);
        return new PageInfo(list);

    }

    public Daily get(Long id) {
        Daily daily = dailyMapper.selectByPrimaryKey(id);
        return daily;
    }

    public void updateByVisitNum(Daily daily) {
        dailyMapper.updateByVisitKey(daily);
    }

    public void saveOrUpdate(Daily daily) {
        if(daily.getId()!=null){
            dailyMapper.updateByPrimaryKey(daily);
        }else{
            daily.setCreateTime(new Date());
            dailyMapper.insert(daily);
        }
    }
}
