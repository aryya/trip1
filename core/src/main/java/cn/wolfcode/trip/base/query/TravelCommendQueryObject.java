package cn.wolfcode.trip.base.query;

import lombok.Getter;
import lombok.Setter;
import org.springframework.util.StringUtils;

@Getter
@Setter
public class TravelCommendQueryObject extends QueryObject {

    //搜索关键字
    private String keyword;

    //查询推荐类型
    private Integer type = -1;


    public String getKeyword() {
        return StringUtils.hasLength(keyword) ? keyword.trim() : null;
    }


}
