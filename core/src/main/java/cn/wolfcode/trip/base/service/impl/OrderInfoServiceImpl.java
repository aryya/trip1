package cn.wolfcode.trip.base.service.impl;

import cn.wolfcode.trip.base.domain.OrderInfo;
import cn.wolfcode.trip.base.domain.Product;
import cn.wolfcode.trip.base.domain.SaleRecord;
import cn.wolfcode.trip.base.mapper.FinanceMapper;
import cn.wolfcode.trip.base.mapper.OrderInfoMapper;
import cn.wolfcode.trip.base.mapper.ProductMapper;
import cn.wolfcode.trip.base.mapper.SaleRecordMapper;
import cn.wolfcode.trip.base.query.OrderInfoQueryObject;
import cn.wolfcode.trip.base.service.IOrderInfoService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.UUID;
//商品定单服务接口

@Service
public class OrderInfoServiceImpl implements IOrderInfoService {

    @Autowired
    private OrderInfoMapper orderInfoMapper;

    @Autowired
    private ProductMapper productMapper;

    @Autowired
    private FinanceMapper financeMapper;

    @Autowired
    private SaleRecordMapper saleRecordMapper;


    public void save(OrderInfo entity, Integer buyNum) {
        //获取商品价格
        Product product = productMapper.selectByPrimaryKey(entity.getProductId());
        BigDecimal expense = product.getPrice().multiply(new BigDecimal(buyNum));

        //更新商品的总退换量
        product.setSalenum(product.getSalenum() + buyNum);
        productMapper.updateByPrimaryKey(product);

        //下单消费 扣除相应的积分
        Long userId = entity.getUserId();
        BigDecimal userIntegral = financeMapper.getUserFinace(userId).getIntegral();
        BigDecimal result = userIntegral.subtract(expense);
        financeMapper.updateIntegral(userId, result);


        //添加消费记录
        SaleRecord saleRecord = new SaleRecord();
        saleRecord.setUserId(userId);
        saleRecord.setProductId(entity.getProductId());
        saleRecord.setSaleTime(new Date());

        saleRecord.setSale(expense);
        saleRecordMapper.insert(saleRecord);


        //将订单总消费设置到对象中
        entity.setExpense(expense);
        //随机生成订单号
        entity.setOrderId(UUID.randomUUID().toString().substring(0, 8));
        //设置下单时间
        entity.setCreateTime(new Date());
        //设置默认为待付款状态
        entity.setStatus(0);
        int insert = orderInfoMapper.insert(entity);

        if (insert != 1) {
            throw new RuntimeException("下单失败");
        }


    }

    public void delete(Long id) {
        orderInfoMapper.deleteByPrimaryKey(id);

    }

    public void update(OrderInfo entity) {
        orderInfoMapper.updateByPrimaryKey(entity);

    }

    public OrderInfo get(Long id) {
        return orderInfoMapper.selectByPrimaryKey(id);
    }

    public List<OrderInfo> listAll() {
        return orderInfoMapper.selectAll();
    }

    public PageInfo<OrderInfo> query(OrderInfoQueryObject qo) {
        PageHelper.startPage(qo.getCurrentPage(), qo.getPageSize(), "o.create_time desc");
        List<OrderInfo> list = orderInfoMapper.selectForList(qo);
        return new PageInfo<OrderInfo>(list);
    }


}
