package cn.wolfcode.trip.base.mapper;

import cn.wolfcode.trip.base.domain.Moments;
import cn.wolfcode.trip.base.query.MomentsQueryObject;
import java.util.List;

public interface MomentsMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Moments record);

    Moments selectByPrimaryKey(Long id);

    List<Moments> selectAll();

    int updateByPrimaryKey(Moments record);

    List selectMomentsByUserId(MomentsQueryObject qo);
}