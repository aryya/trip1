package cn.wolfcode.trip.base.service;

import cn.wolfcode.trip.base.domain.Sign;
import cn.wolfcode.trip.base.query.SignQueryObject;

import java.util.List;

public interface ISignService {
    /**
     * 根据当前用户的id查询当月的签到数
     * @param qo 查询条件
     * @return 返回一个签到天数的list集合
     */
    List<Sign> getMonthSign(SignQueryObject qo);

    /**
     * 根据当前用户添加新的签到信息
     * @param qo 签到信息
     * @return 受影响行数
     */
    Sign creatNewSign(SignQueryObject qo);

    /**
     * 为当前用户增加积分
     * @param currentUserId 当前用户的id
     * @param integral 添加的积分值
     */
    void addIntegral(Long currentUserId, Integer integral);
}
