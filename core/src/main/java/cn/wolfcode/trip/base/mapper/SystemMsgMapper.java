package cn.wolfcode.trip.base.mapper;

import cn.wolfcode.trip.base.domain.SystemMsg;
import java.util.List;

public interface SystemMsgMapper {
    int deleteByPrimaryKey(Long id);

    int insert(SystemMsg record);

    SystemMsg selectByPrimaryKey(Long id);

    List<SystemMsg> selectAll();

    int updateByPrimaryKey(SystemMsg record);

    List<SystemMsg> getAllSytemMsgByCurrentId(Long currentId);

    void updateState(SystemMsg systemMsg);


    int getSystemMsgCount(Long currentId);
}