package cn.wolfcode.trip.base.mapper;

import cn.wolfcode.trip.base.domain.Follow;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface FollowMapper {

    int insert(Follow record);

    List<Follow> selectAll();

    Follow selectMutualRelationship(@Param("userId") Long userId, @Param("authorId") Long authorId);

    void insertMutualRelationship(@Param("userId") Long userId, @Param("authorId") Long authorId);

    int deleteMutualRelationship(@Param("userId") Long userId, @Param("authorId") Long authorId);

    int selectFollowNum(@Param("userId") Long userId);

    int selectFollowerNum(@Param("authorId") Long authorId);
}