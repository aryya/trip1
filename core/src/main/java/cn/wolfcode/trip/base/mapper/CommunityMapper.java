package cn.wolfcode.trip.base.mapper;

import cn.wolfcode.trip.base.domain.Community;
import cn.wolfcode.trip.base.query.CommunityQueryObject;
import cn.wolfcode.trip.base.query.QueryObject;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CommunityMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Community record);

    Community selectByPrimaryKey(Long id);

    List<Community> selectAll();

    int updateByPrimaryKey(Community record);

    List queryListCommunities(CommunityQueryObject qo);

    Community selectCommunityById(@Param("id") Long id);
}