package cn.wolfcode.trip.base.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * 日报评论
 */
@Setter
@Getter
public class DailyComment extends BaseDomain{
    // 内容
    private String comment;
    // 评论者
    private User user;
    // 日报
    private Daily daily;
    // 状态
    private Integer state=0;
    //创建时间
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date creatTime;
}