package cn.wolfcode.trip.base.domain;

import lombok.*;

/**
 * 注册用户
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Data
public class User extends BaseDomain {
    //性别赋予int类型表示,方便传数据
    public static final Integer FEMALE = 0;
    public static final Integer MALE = 1;
    public static final Integer SECRECY = -1;

    //邮箱
    private String email;
    //别名
    private String nickName;
    //密码
    private String password;
    //地点
    private String place;
    //头像
    private String headImgUrl;
    //性别
    private Integer gender = -1;

    public String getGenderName() {
        String name = null;
        if (gender == 0) {
            name = "女";
        } else if (gender == 1) {
            name = "男";
        } else {
            name = "保密";
        }
        return name;
    }

    //背景
    private String coverImgUrl;
    //签名
    private String sign;
}