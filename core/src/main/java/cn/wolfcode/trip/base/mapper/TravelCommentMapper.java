package cn.wolfcode.trip.base.mapper;

import cn.wolfcode.trip.base.domain.TravelComment;
import cn.wolfcode.trip.base.domain.TravelContent;
import cn.wolfcode.trip.base.query.TravelCommentQueryObject;
import cn.wolfcode.trip.base.query.UserCommentsQueryObject;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface TravelCommentMapper {
    int deleteByPrimaryKey(Long id);

    int insert(TravelComment record);

    TravelComment selectByPrimaryKey(Long id);

    List<TravelComment> selectAll();

    int updateByPrimaryKey(TravelComment record);

    List<TravelComment> selectCommentByTravelId(TravelCommentQueryObject qo);

    List queryCommentsByUserId(UserCommentsQueryObject qo);

    List<TravelContent> listTravelCommentByUserId(Long userId);

    void insertUnReadComment(@Param("userId") Long userId, @Param("id") Long id);
}