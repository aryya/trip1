package cn.wolfcode.trip.base.domain;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class LocationSituation extends BaseDomain{

    //景点ID
    private ScenicSpotTicket scenic;
    //概述
    private String summarize;
    //电话
    private String phone;
    //用时参考
    private String timeReference;
    //交通
    private String traffic;
    //门票
    private String tickets;
    //开放时间
    private String openTime;
    //景点位置
    private String location;

}