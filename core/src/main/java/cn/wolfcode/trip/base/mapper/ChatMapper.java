package cn.wolfcode.trip.base.mapper;

import cn.wolfcode.trip.base.domain.Chat;
import cn.wolfcode.trip.base.query.ChatQueryObject;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ChatMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Chat record);

    Chat selectByPrimaryKey(Long id);

    List<Chat> selectAll();

    int updateByPrimaryKey(Chat record);

    int selectUnReadMsgNum(Integer userId);

    List selectChatHistory(ChatQueryObject qo);

    List selectNewUnReadMsg(ChatQueryObject qo);

    void updateChatState(@Param("state")Boolean state, @Param("id") Long id);
}