package cn.wolfcode.trip.base.domain;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
@Setter
@Getter
public class ScenicSpotTicket extends BaseDomain{
    //景区
    private Scenics place;
    //标题
    private String title;
    //封面
    private String coverUrl;
    //景区价格
    private BigDecimal salary;

}