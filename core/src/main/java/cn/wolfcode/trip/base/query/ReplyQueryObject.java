package cn.wolfcode.trip.base.query;

import lombok.Getter;
import lombok.Setter;
import org.springframework.util.StringUtils;

/**
 * 二级评论的qo
 */
@Getter
@Setter
public class ReplyQueryObject extends QueryObject{
    //排序功能
    private String orderBy;
    // 攻略id
    private Long commentId;
    public String getOrderBy() {
        return StringUtils.hasLength(orderBy) ? orderBy : null;
    }
}
