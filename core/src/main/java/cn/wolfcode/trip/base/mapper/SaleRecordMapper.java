package cn.wolfcode.trip.base.mapper;

import cn.wolfcode.trip.base.domain.SaleRecord;
import cn.wolfcode.trip.base.query.QueryObject;

import java.util.List;

public interface SaleRecordMapper {
    int deleteByPrimaryKey(Long id);

    int insert(SaleRecord record);

    SaleRecord selectByPrimaryKey(Long id);

    List<SaleRecord> selectAll();

    int updateByPrimaryKey(SaleRecord record);

    List<SaleRecord> selectForList(QueryObject qo);
}