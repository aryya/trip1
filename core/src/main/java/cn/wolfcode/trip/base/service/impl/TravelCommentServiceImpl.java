package cn.wolfcode.trip.base.service.impl;

import cn.wolfcode.trip.base.domain.Travel;
import cn.wolfcode.trip.base.domain.TravelComment;
import cn.wolfcode.trip.base.domain.TravelContent;
import cn.wolfcode.trip.base.domain.User;
import cn.wolfcode.trip.base.mapper.TravelCommentMapper;
import cn.wolfcode.trip.base.mapper.TravelMapper;
import cn.wolfcode.trip.base.query.TravelCommentQueryObject;
import cn.wolfcode.trip.base.service.ITravelCommentService;
import cn.wolfcode.trip.base.util.UserContext;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class TravelCommentServiceImpl implements ITravelCommentService{
    @Autowired
    private TravelCommentMapper commentMapper;
    @Autowired
    private TravelMapper travelMapper;
    //查询所有游记评论
    public PageInfo getCommentByTravelId(TravelCommentQueryObject qo) {
        PageHelper.startPage(qo.getCurrentPage(),qo.getPageSize(),qo.getOrderBy());
        List<TravelComment> list = commentMapper.selectCommentByTravelId(qo);
        return new PageInfo(list);
    }
    // 新增游记评论
    public void save(TravelComment comment) {
        User user = UserContext.getUser();
        if(user==null){
            throw new RuntimeException("用户没有登录");
        }
        comment.setUser(user);
        comment.setCarentTime(new Date());
        commentMapper.insert(comment);
        //查询该游记作者的id
        Long userId = travelMapper.selectantherId(comment.getTravel().getId());
        commentMapper.insertUnReadComment(userId,comment.getId());

    }
    // 查询未读的游记评论
    public List<TravelContent> listTravelCommentByUserId(Long userId) {
        return commentMapper.listTravelCommentByUserId(userId);
    }
}
