package cn.wolfcode.trip.base.mapper;

import cn.wolfcode.trip.base.domain.UserAddress;
import java.util.List;

public interface UserAddressMapper {
    int deleteByPrimaryKey(Long addressId);

    int insert(UserAddress record);

    UserAddress selectByPrimaryKey(Long id);

    List<UserAddress> selectAll(Long userId);

    int updateByPrimaryKey(UserAddress record);

    void updateByUserId(Long userId);

    void updateDefaultAddress(Long addressId);
}