package cn.wolfcode.trip.base.domain;

import cn.wolfcode.trip.base.util.JsonUtil;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

@Getter
@Setter
public class StrategyComment extends BaseDomain {
    private static final int STATE_NORMAL = 0;//正常
    private static final int STATE_COMMEND = 1;//推荐
    private static final int STATE_DISABLE = -1;//禁止

    //评论人id
    private User user;
    //评论时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;
    //评论内容
    private String content;
    //评论图片
    private String imgUrls;
    //星星数量
    private Integer starNum;
    //所属攻略id
    private Strategy strategy;
    //状态
    private Integer state = STATE_NORMAL;
    //头条推荐时间
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date commendTime;

    public String getStateName() {
        String temp = "";
        switch (state) {
            case STATE_NORMAL:
                temp = "正常";
                break;
            case STATE_COMMEND:
                temp = "推荐";
                break;
            case STATE_DISABLE:
                temp = "禁止";
                break;
        }
        return temp;
    }

    public String getJsonData() {
        HashMap<Object, Object> map = new HashMap();
        map.put("id", id);
        map.put("state", state);
        return JsonUtil.getJsonString(map);
    }

    //用于前端取值,图片显示在评论下方
    public String[] getImgArr() {
        if (StringUtils.hasLength(imgUrls)) {
            return imgUrls.split(";");
        }
        return null;
    }

}