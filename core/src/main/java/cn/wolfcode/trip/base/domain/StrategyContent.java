package cn.wolfcode.trip.base.domain;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StrategyContent extends BaseDomain {
    //内容
    private String content;

}