package cn.wolfcode.trip.base.domain;

import lombok.Getter;
import lombok.Setter;


//用户的收货地址
@Setter
@Getter
public class UserAddress extends BaseDomain {

    private Long uId; //用户id

    private String name; //收货人姓名

    private String phone; //收货人手机号

    private String address; //收件人地址

    private Integer defaultAddr = 0; //收货人地址 0 表示非默认 1表示默认

}