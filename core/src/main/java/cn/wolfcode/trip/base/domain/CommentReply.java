package cn.wolfcode.trip.base.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * 二级评论的类
 */
@Getter
@Setter
public class CommentReply extends BaseDomain {
    // 攻略评论id
    private Long commentId;
    // 用户
    private User user;
    // 评论内容
    private String content;
    // 上一级评论的id
    private Long parentId;
    // 创建时间
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date carentTime;
}