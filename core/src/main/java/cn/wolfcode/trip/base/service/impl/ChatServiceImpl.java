package cn.wolfcode.trip.base.service.impl;

import cn.wolfcode.trip.base.domain.Chat;
import cn.wolfcode.trip.base.mapper.ChatMapper;
import cn.wolfcode.trip.base.mapper.UserMapper;
import cn.wolfcode.trip.base.query.ChatQueryObject;
import cn.wolfcode.trip.base.query.QueryObject;
import cn.wolfcode.trip.base.service.IChatService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Set;

@Service
public class ChatServiceImpl implements IChatService {

    @Autowired
    private ChatMapper chatMapper;

    @Autowired
    private UserMapper userMapper;

    public int getUnReadMsgNum(Integer userId) {
        return chatMapper.selectUnReadMsgNum(userId);
    }

    public PageInfo ListReceivers(ChatQueryObject qo) {
        qo.setPageSize(10);
        PageHelper.startPage(qo.getCurrentPage(), qo.getPageSize());
        List list = userMapper.selectListReceivers(qo);
        return new PageInfo(list);
    }

    public PageInfo getChatHistory(ChatQueryObject qo) {
        PageHelper.startPage(qo.getCurrentPage(), qo.getPageSize(), qo.getOrderBy());
        List<Chat> list = chatMapper.selectChatHistory(qo);
/*
        if(list!=null){
            for (Chat chat : list) {
                chatMapper.updateChatState(false,chat.getId());
            }
        }*/

        return new PageInfo(list);
    }

    public Chat createNewChat(Chat chat) {
        //添加一个新聊天记录的时间
        chat.setMsgDate(new Date());
        chat.setState(true);
        chatMapper.insert(chat);
        return chat;
    }

    /**
     * 获取新的信息
     * @param qo 查询条件
     * @return
     */
    public List getNewUnReadMsg(ChatQueryObject qo) {
        List<Chat> list = chatMapper.selectNewUnReadMsg(qo);
        return list;
    }

    public void updateChatState(List<Chat> list) {
        for (Chat chat : list) {
            chat.setState(false);
            chatMapper.updateByPrimaryKey(chat);
            //chatMapper.updateChatState(false,chat.getId());
        }
    }

}
