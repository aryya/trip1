package cn.wolfcode.trip.base.service.impl;

import cn.wolfcode.trip.base.domain.GoodMe;
import cn.wolfcode.trip.base.mapper.GoodMeMapper;
import cn.wolfcode.trip.base.query.GoodMeQueryObject;
import cn.wolfcode.trip.base.query.QueryObject;
import cn.wolfcode.trip.base.service.IGoodMeService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GoodMeServiceImpl implements IGoodMeService {
    @Autowired
    private GoodMeMapper goodMeMapper;

    public int getGoodMeCount(Long currentId) {
        return goodMeMapper.selectGoodMeCount(currentId);
    }

    public PageInfo getAllTravelGoode(GoodMeQueryObject qo) {
        qo.setPageSize(0);
        PageHelper.startPage(qo.getCurrentPage(), qo.getPageSize(), qo.getOrderBy());
        List list = goodMeMapper.getAllTravelGoode(qo);
        return new PageInfo(list);

    }

    public PageInfo<GoodMe> getAllCommentGoode(GoodMeQueryObject qo) {
        qo.setPageSize(0);
        PageHelper.startPage(qo.getCurrentPage(), qo.getPageSize(), qo.getOrderBy());
        List list = goodMeMapper.getAllCommentGoode(qo);
        return new PageInfo(list);
    }

    public void updateState(GoodMe goodMe) {
        goodMeMapper.updateState(goodMe);
    }
}
