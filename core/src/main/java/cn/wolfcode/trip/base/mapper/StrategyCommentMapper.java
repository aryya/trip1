package cn.wolfcode.trip.base.mapper;

import cn.wolfcode.trip.base.domain.StrategyComment;
import cn.wolfcode.trip.base.query.StrategyCommentQueryObject;
import cn.wolfcode.trip.base.query.UserCommentsQueryObject;
import org.apache.ibatis.annotations.Param;
import cn.wolfcode.trip.base.query.QueryObject;
import cn.wolfcode.trip.base.query.StrategyCommentQueryObject;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface StrategyCommentMapper {
    int deleteByPrimaryKey(Long id);

    int insert(StrategyComment record);

    StrategyComment selectByPrimaryKey(Long id);

    List<StrategyComment> selectAll();

    int updateByPrimaryKey(StrategyComment record);

    List selectForList(StrategyCommentQueryObject qo);

    int updateStausById(StrategyComment strategyComment);

    void insertCommentTagRelation(@Param("commentId") Long commentId, @Param("tagId") Long tagId);

    List queryCommentsByUserId(UserCommentsQueryObject qo);

    List selectCommetsByUserId(QueryObject qo);

    Long selectStrategyCommentAnthorId(Long commentId);

}