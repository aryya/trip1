package cn.wolfcode.trip.base.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;

//商品消费相关
@Setter
@Getter
public class SaleRecord extends BaseDomain {

    private Long productId; //商品id

    private String productName; //商品名称

    private String coverUrl; //商品封面

    private Long userId; //消费者id

    private String userName; //消费者姓名

    private BigDecimal sale; //消费金额

    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date saleTime;

}