package cn.wolfcode.trip.base.mapper;

import cn.wolfcode.trip.base.domain.DailyComment;
import cn.wolfcode.trip.base.query.DailyCommentQueryObject;
import com.github.pagehelper.PageInfo;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

public interface DailyCommentMapper {
    int deleteByPrimaryKey(Long id);

    int insert(DailyComment record);

    DailyComment selectByPrimaryKey(Long id);

    List<DailyComment> selectAll();

    int updateByPrimaryKey(DailyComment record);

    List<DailyComment> queryForList(DailyCommentQueryObject qo);
}