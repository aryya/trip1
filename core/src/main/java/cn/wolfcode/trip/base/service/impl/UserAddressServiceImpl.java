package cn.wolfcode.trip.base.service.impl;

import cn.wolfcode.trip.base.domain.UserAddress;
import cn.wolfcode.trip.base.mapper.UserAddressMapper;
import cn.wolfcode.trip.base.service.IUserAddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserAddressServiceImpl implements IUserAddressService {

    @Autowired
    private UserAddressMapper userAddressMapper;

    public void save(UserAddress entity) {
//       设置收货地址的状态
        entity.setDefaultAddr(0);
        userAddressMapper.insert(entity);

    }

    public void delete(Long addressId) {
        userAddressMapper.deleteByPrimaryKey(addressId);
    }

    public void update(UserAddress entity) {
        userAddressMapper.updateByPrimaryKey(entity);
    }

    public UserAddress get(Long id) {
        return userAddressMapper.selectByPrimaryKey(id);
    }

    public List<UserAddress> listAll(Long userId) {
        return userAddressMapper.selectAll(userId);
    }

    public void updateDefaultAddress(Long userId, Long addressId) {
        //将该用户所有的default全部设为0 即非默认
        UserAddress userAddress = new UserAddress();
        userAddress.setUId(userId);
        userAddress.setDefaultAddr(0);
        userAddressMapper.updateByUserId(userId);
        userAddressMapper.updateDefaultAddress(addressId);
    }
}
