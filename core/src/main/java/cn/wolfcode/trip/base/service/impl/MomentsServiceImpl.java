package cn.wolfcode.trip.base.service.impl;

import cn.wolfcode.trip.base.domain.Moments;
import cn.wolfcode.trip.base.domain.MomentsComment;
import cn.wolfcode.trip.base.mapper.MomentsCommentMapper;
import cn.wolfcode.trip.base.mapper.MomentsMapper;
import cn.wolfcode.trip.base.query.MomentsCommentQueryObject;
import cn.wolfcode.trip.base.query.MomentsQueryObject;
import cn.wolfcode.trip.base.service.IMomentsService;
import cn.wolfcode.trip.base.util.UserContext;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Date;
import java.util.List;

@Service
public class MomentsServiceImpl implements IMomentsService {

    @Autowired
    private MomentsMapper momentsMapper;
    @Autowired
    private MomentsCommentMapper commentMapper;


    /**
     * 查询朋友圈所有动态
     *
     * @param qo
     * @return
     */
    public PageInfo queryMomentsByUserId(MomentsQueryObject qo) {
        PageHelper.startPage(qo.getCurrentPage(), qo.getPageSize(), qo.getOrderBy());
        List list = momentsMapper.selectMomentsByUserId(qo);
        return new PageInfo(list);
    }

    /**
     * 当前登录用户新增动态
     *
     * @param moments
     */
    public void save(Moments moments) {
        //保存之前设置用户
        moments.setUser(UserContext.getUser());
        //设置发朋友圈的时间
        moments.setCreateTime(new Date());
        momentsMapper.insert(moments);
    }

    /**
     * 查询朋友圈每条动态所有的评论
     *
     * @param qo
     * @return pageInfo
     */
    public PageInfo queryCommentsById(MomentsCommentQueryObject qo) {
        PageHelper.startPage(qo.getCurrentPage(), qo.getPageSize(), qo.getOrderBy());
        List list = commentMapper.selectCommentsById(qo);
        return new PageInfo(list);
    }

    /**
     * 新增评论内容
     *
     * @param momentsComment
     * @return
     */
    public void saveComments(MomentsComment momentsComment) {
        momentsComment.setUser(UserContext.getUser());
        //设置发朋友圈的时间
        momentsComment.setCreateTime(new Date());
        commentMapper.insert(momentsComment);
    }


}
