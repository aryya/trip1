package cn.wolfcode.trip.base.service;

import cn.wolfcode.trip.base.domain.GoodMe;
import cn.wolfcode.trip.base.query.GoodMeQueryObject;
import com.github.pagehelper.PageInfo;

import java.util.List;

public interface IGoodMeService {
    /**
     * 通过当前用户的id查询未读的点赞信息
     * @param currentId 当前用户的id
     * @return 未读的点赞数
     */
    int getGoodMeCount(Long currentId);

    /**
     * 获取当前用户所有未读的点赞信息
     * @param qo 查询的条件
     * @return
     */
    PageInfo getAllTravelGoode(GoodMeQueryObject qo);

    /**
     * 获取当前用户的所有未读的评论点赞信息
     * @param qo 查询条件
     * @return
     */
    PageInfo getAllCommentGoode(GoodMeQueryObject qo);

    /**
     * 更新点赞的状态
     * @param goodMe
     */
    void updateState(GoodMe goodMe);
}
