package cn.wolfcode.trip.base.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Getter
@Setter
public class GoodMe extends BaseDomain {
    //点赞的人
    private User user;
    //被点赞的人
    private User goodUser;
    //评论的id
    private StrategyComment comment;
    //游记的id
    private Travel travel;
    //点赞的时间
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;
    //是否已读
    private Long state;

}