package cn.wolfcode.trip.base.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import java.util.Date;

@Getter
@Setter
public class MomentsComment extends BaseDomain {

    //评论人id
    private User user;
    //发朋友圈动态的id
    private MomentsComment moments;
    //二级回复评论者的id
    private User comments;
    //评论内容
    private String comment;
    //评论创建时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;

}