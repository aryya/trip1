package cn.wolfcode.trip.base.query;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GoodMeQueryObject extends QueryObject {
    private Long currentId;
}
