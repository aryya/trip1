package cn.wolfcode.trip.base.domain;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Tag extends BaseDomain {

    //标签内容
    private String name;

}