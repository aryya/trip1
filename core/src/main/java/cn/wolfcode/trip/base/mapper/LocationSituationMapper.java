package cn.wolfcode.trip.base.mapper;

import cn.wolfcode.trip.base.domain.LocationSituation;
import java.util.List;

public interface LocationSituationMapper {
    int deleteByPrimaryKey(Long id);

    int insert(LocationSituation record);

    LocationSituation selectByPrimaryKey(Long id);

    List<LocationSituation> selectAll();

    int updateByPrimaryKey(LocationSituation record);
}