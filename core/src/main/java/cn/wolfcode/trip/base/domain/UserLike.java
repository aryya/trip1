package cn.wolfcode.trip.base.domain;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class UserLike {
    private Long id;

    private Travel travel;

    private StrategyDetail strategyDetail;

    private Daily daily;

    private User user;
    private TravelContent travelContent;
    private StrategyContent strategyContent;

}