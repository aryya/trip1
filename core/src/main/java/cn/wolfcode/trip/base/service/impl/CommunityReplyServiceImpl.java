package cn.wolfcode.trip.base.service.impl;

import cn.wolfcode.trip.base.domain.CommunityReply;
import cn.wolfcode.trip.base.domain.StrategyComment;
import cn.wolfcode.trip.base.domain.User;
import cn.wolfcode.trip.base.mapper.CommentReplyMapper;
import cn.wolfcode.trip.base.mapper.CommunityReplyMapper;
import cn.wolfcode.trip.base.query.CommunityReplyQueryObject;
import cn.wolfcode.trip.base.query.QueryObject;
import cn.wolfcode.trip.base.service.ICommunityReplyService;
import cn.wolfcode.trip.base.util.UserContext;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * 社区评论的service
 */
@Service
public class CommunityReplyServiceImpl implements ICommunityReplyService {
    @Autowired
    private CommunityReplyMapper replyMapper;

    /**
     * 回复区列表
     * @param qo
     * @return
     */
    public PageInfo listAllReplyByCommunityId(CommunityReplyQueryObject qo) {
        PageHelper.startPage(qo.getCurrentPage(),qo.getPageSize(),qo.getOrderBy());
        List list = replyMapper.listAllReplyByCommunityId(qo);
        return new PageInfo(list);
    }

    /**
     * 添加一条回复
     * @param communityReply
     */
    public void saveReply(CommunityReply communityReply) {
        User user = UserContext.getUser();
        if(user==null){
            throw new RuntimeException("没有登录");
        }
        communityReply.setUser(user);
        communityReply.setCreatTime(new Date());
        replyMapper.insert(communityReply);
    }

    /**
     * 统计评论数
     * @param id
     * @return
     */
    public int countRepliseByCommunityId(Long id) {
        return replyMapper.countRepliseByCommunityId(id);
    }
}
