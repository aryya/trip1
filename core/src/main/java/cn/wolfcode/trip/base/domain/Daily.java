package cn.wolfcode.trip.base.domain;

import cn.wolfcode.trip.base.util.JsonResult;
import cn.wolfcode.trip.base.util.JsonUtil;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.HashMap;

@Setter
@Getter
public class Daily extends BaseDomain{

    //标题
    private String title;
    //副标题
    private String subTitle;
    //封面
    private String coverUrl;
    //创建时间
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;
    //浏览点击数
    private Integer num;
    //状态
    private Boolean state;
    //日报内容
    private DailyContent dailyContent;




    public String getJsonData() {
        HashMap<Object, Object> map = new HashMap();
        map.put("id", id);
        map.put("state", state);
        map.put("title",title);
        map.put("subTitle",subTitle);
        map.put("coverUrl",coverUrl);
        map.put("createTime",createTime);
        map.put("num",num);
        if(dailyContent!=null){
            map.put("ContentId",dailyContent.getId());
            map.put("ContentName",dailyContent.getContent());

        }


        return JsonUtil.getJsonString(map);
    }
}