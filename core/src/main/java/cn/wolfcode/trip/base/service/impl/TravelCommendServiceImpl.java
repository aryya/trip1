package cn.wolfcode.trip.base.service.impl;

import cn.wolfcode.trip.base.domain.TravelCommend;
import cn.wolfcode.trip.base.mapper.TravelCommendMapper;
import cn.wolfcode.trip.base.query.TravelCommendQueryObject;
import cn.wolfcode.trip.base.service.ITravelCommendService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TravelCommendServiceImpl implements ITravelCommendService {

    @Autowired
    private TravelCommendMapper commendMapper;


    /**
     * 查询游记推荐管理分页的内容
     *
     * @param qo
     * @return
     */
    public PageInfo selectForList(TravelCommendQueryObject qo) {
        PageHelper.startPage(qo.getCurrentPage(), qo.getPageSize());
        List list = commendMapper.queryForList(qo);
        return new PageInfo(list);
    }

    /**
     * 保存或更新模态框数据
     *
     * @param travelCommend
     */
    public void saveOrUpdate(TravelCommend travelCommend) {

        if (travelCommend.getId() != null) {
            commendMapper.updateByPrimaryKey(travelCommend);
        } else {
            commendMapper.insert(travelCommend);
        }
    }

    /**
     * 冠军游记APP
     * @param qo
     * @return
     */
    public PageInfo queryForApp(TravelCommendQueryObject qo) {
        PageHelper.startPage(qo.getCurrentPage(), qo.getPageSize());
        List list = commendMapper.selectForAppList(qo);
        return new PageInfo(list);
    }
}
