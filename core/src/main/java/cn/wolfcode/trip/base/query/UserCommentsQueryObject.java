package cn.wolfcode.trip.base.query;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class UserCommentsQueryObject extends QueryObject {
    private Long userId;
    private Long state;
}
