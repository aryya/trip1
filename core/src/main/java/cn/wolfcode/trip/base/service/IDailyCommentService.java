package cn.wolfcode.trip.base.service;

import cn.wolfcode.trip.base.domain.DailyComment;
import cn.wolfcode.trip.base.query.DailyCommentQueryObject;
import com.github.pagehelper.PageInfo;

public interface IDailyCommentService {
    /**
     * 查询日报评论
     * @param qo
     * @return
     */
    PageInfo query(DailyCommentQueryObject qo);

    void saveCommentByDaily(DailyComment comment);
}
