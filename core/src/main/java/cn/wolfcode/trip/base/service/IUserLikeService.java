package cn.wolfcode.trip.base.service;

import cn.wolfcode.trip.base.query.UserQueryObject;

import java.util.List;
import java.util.Map;

public interface IUserLikeService {
    /**
     * 用户收藏查询
     *
     */

    List selectByUserId(UserQueryObject qo);


    List<Map<String ,Object>> selectAll(Long userId);

    /**
     * 查询攻略
     */

    List getStrategys(UserQueryObject qo);

    /**
     * 查询游记
     */
    List getTravel(UserQueryObject qo);

    /**
     * 查询日报
     */
    List getDaily(UserQueryObject qo);

}
