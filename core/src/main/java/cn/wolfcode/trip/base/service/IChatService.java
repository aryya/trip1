package cn.wolfcode.trip.base.service;

import cn.wolfcode.trip.base.domain.Chat;
import cn.wolfcode.trip.base.query.ChatQueryObject;
import com.github.pagehelper.PageInfo;

import java.util.List;

public interface IChatService {

    /**
     * 获取当前用户未读的信息数
     *
     * @param userId 用户的id
     * @return 当前用户未读的信息数
     */
    int getUnReadMsgNum(Integer userId);

    /**
     * 通过当前用户的id查询当前用户私信的通信列表
     *
     * @param qo 当前用户的id所在的查询条件类
     * @return pageInfo对象
     */
    PageInfo ListReceivers(ChatQueryObject qo);

    /**
     * 查询聊天记录
     *
     * @param qo
     * @return
     */
    PageInfo getChatHistory(ChatQueryObject qo);

    /**
     * 添加新的聊天信息
     *
     * @param chat 要添加的聊天信息对象
     * @return 返回添加的聊天信息对象
     */
    Chat createNewChat(Chat chat);

    /**
     * 用于定时器轮询查询最新的消息
     *
     * @param qo 查询条件
     * @return 返回最新的消息
     */
    List getNewUnReadMsg(ChatQueryObject qo);

    void updateChatState(List<Chat> list);
}
