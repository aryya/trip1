package cn.wolfcode.trip.base.service;

import cn.wolfcode.trip.base.domain.CommunityReply;
import cn.wolfcode.trip.base.domain.StrategyComment;
import cn.wolfcode.trip.base.query.CommunityReplyQueryObject;
import cn.wolfcode.trip.base.query.QueryObject;
import com.github.pagehelper.PageInfo;

import java.util.List;

public interface ICommunityReplyService {
    /**
     * 获取所有的回复
     * @param qo
     * @return
     */
    PageInfo listAllReplyByCommunityId(CommunityReplyQueryObject qo);

    /**
     * 添加一条回复
     * @param communityReply
     */
    void saveReply(CommunityReply communityReply);

    /**
     * 统计回复数量
     * @param id
     * @return
     */
    int countRepliseByCommunityId(Long id);
}
