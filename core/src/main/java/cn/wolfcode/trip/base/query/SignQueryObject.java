package cn.wolfcode.trip.base.query;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SignQueryObject {
    //根据当前用户的id查询对应的当月签到数
    private Long currentUserId;
    //当前月
    private Long currentMonth;
}
