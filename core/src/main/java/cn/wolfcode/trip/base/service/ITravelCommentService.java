package cn.wolfcode.trip.base.service;

import cn.wolfcode.trip.base.domain.TravelComment;
import cn.wolfcode.trip.base.domain.TravelContent;
import cn.wolfcode.trip.base.query.TravelCommentQueryObject;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * 游记评论的service
 */
public interface ITravelCommentService {
    // 查询游记评论
    PageInfo getCommentByTravelId(TravelCommentQueryObject qo);
    // 新增游记评论
    void save(TravelComment comment);
    // 查询所有未读的游记评论
    List<TravelContent> listTravelCommentByUserId(Long userId);
}
