package cn.wolfcode.trip.base.mapper;

import cn.wolfcode.trip.base.domain.ExchangeRate;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;

public interface ExchangeRateMapper {
    int deleteByPrimaryKey(Long id);

    int insert(ExchangeRate record);

    ExchangeRate selectByPrimaryKey(Long id);

    List<ExchangeRate> selectAll();

    int updateByPrimaryKey(ExchangeRate record);

    BigDecimal selectInSidePriceById(@Param("id") Long id);

    String selectRatesMinName(@Param("exchangeRateId") Long exchangeRateId);
}