package cn.wolfcode.trip.base.service;


import cn.wolfcode.trip.base.domain.Moments;
import cn.wolfcode.trip.base.domain.MomentsComment;
import cn.wolfcode.trip.base.query.MomentsCommentQueryObject;
import cn.wolfcode.trip.base.query.MomentsQueryObject;
import com.github.pagehelper.PageInfo;


public interface IMomentsService {

    PageInfo queryMomentsByUserId(MomentsQueryObject qo);

    void save(Moments moments);

    PageInfo queryCommentsById(MomentsCommentQueryObject qo);

    void saveComments(MomentsComment momentsComment);


}
