package cn.wolfcode.trip.base.service.impl;

import cn.wolfcode.trip.base.domain.CommentReply;
import cn.wolfcode.trip.base.domain.User;
import cn.wolfcode.trip.base.mapper.CommentReplyMapper;
import cn.wolfcode.trip.base.mapper.StrategyCommentMapper;
import cn.wolfcode.trip.base.query.ReplyQueryObject;
import cn.wolfcode.trip.base.service.ICommentReplyService;
import cn.wolfcode.trip.base.util.UserContext;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class CommentReplyServiceImpl implements ICommentReplyService {
    @Autowired
    private CommentReplyMapper replyMapper;
    @Autowired
    private StrategyCommentMapper commentMapper;
    /**
     * 查询所有一级评论
     */
    public PageInfo listReplyByCommentId(ReplyQueryObject qo) {
        PageHelper.startPage(qo.getCurrentPage(),qo.getPageSize(),qo.getOrderBy());
        List<CommentReply> list = replyMapper.selectReplyByCommentId(qo);
        return new PageInfo(list);
    }

    /**
     * 查询二级评论
     * @param parentId
     * @return
     */
    public List<CommentReply> listByParentId(Long parentId) {
        return replyMapper.listByParentId(parentId);
    }

    /**
     * 保存评论
     * @param reply
     */
    public void save(CommentReply reply) {
        reply.setCarentTime(new Date());
        User user = UserContext.getUser();
        reply.setUser(user);
        replyMapper.insert(reply);
        // 查询该攻略评论的作者
        Long id=commentMapper.selectStrategyCommentAnthorId(reply.getCommentId());
        replyMapper.insertCommentIdAndUserId(reply.getId(),id);
    }

    /**
     * 统计评论数
     */
    public int selectCountReply(Long commentId) {
        return replyMapper.selectCountReply(commentId);
    }

    /**
     * 统计所有的未读攻略评论数
     * @param userId
     * @return
     */
    public List<CommentReply> listStrategyCommentByUserId(Long userId) {
        return replyMapper.listStrategyCommentByUserId(userId);
    }
}
