package cn.wolfcode.trip.base.service.impl;

import cn.wolfcode.trip.base.domain.Community;
import cn.wolfcode.trip.base.mapper.CommunityMapper;
import cn.wolfcode.trip.base.query.CommunityQueryObject;
import cn.wolfcode.trip.base.query.QueryObject;
import cn.wolfcode.trip.base.service.ICommunityService;
import cn.wolfcode.trip.base.util.UserContext;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * 社区问题service
 */
@Service
public class CommunityServiceImpl implements ICommunityService{
    @Autowired
    private CommunityMapper communityMapper;

    /**
     * 查询所有的社区问题
     * @param qo
     * @return
     */
    public PageInfo listAllCommunities(CommunityQueryObject qo) {
        PageHelper.startPage(qo.getCurrentPage(),qo.getPageSize(),qo.getOrderBy());
        List list =  communityMapper.queryListCommunities(qo);
        return new PageInfo(list);
    }

    /**
     * 添加社区问题
     * @param community
     */
    public void saveProblem(Community community) {
        community.setUser(UserContext.getUser());
        community.setCreatTime(new Date());
        community.setState(Community.STATUS_NORMAL);
        communityMapper.insert(community);
    }

    /**
     * 查询单条社区问题
     * @param id
     * @return
     */
    public Community getCommunityById(Long id) {
        return communityMapper.selectCommunityById(id);
    }
}
