package cn.wolfcode.trip.base.mapper;

import cn.wolfcode.trip.base.domain.Finance;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;

public interface FinanceMapper {

    int insert(Finance record);

    List<Finance> selectAll();

    Finance getFinanceByUserId(Long userId);

    Finance getUserFinace(Long userId);

    int updateIntegral(@Param("userId") Long userId, @Param("result") BigDecimal result);
}