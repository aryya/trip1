package cn.wolfcode.trip.base.service;

import cn.wolfcode.trip.base.domain.Region;
import cn.wolfcode.trip.base.query.StrategyQueryObject;
import java.util.List;

public interface IRegionService {

    int deleteByPrimaryKey(Long id);

    int insert(Region record);

    Region selectByPrimaryKey(Long id);

    List<Region> selectAll(Integer state);

    int updateByPrimaryKey(Region record);

    List<Region> selectByParentId(Long parentId);

    void updateStatus(Long id, Integer state);

    List<Region> listAll(StrategyQueryObject qo);
}
