package cn.wolfcode.trip.base.query;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class TravelCommentQueryObject extends QueryObject{
    private Long travelId;
}
