package cn.wolfcode.trip.base.query;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MomentsCommentQueryObject extends QueryObject {

    //评论人id
    private Long userId;
    //朋友圈动态人的id
    private Long momentsId;
    //被回复评论者的id
    private Long commentId;


}
