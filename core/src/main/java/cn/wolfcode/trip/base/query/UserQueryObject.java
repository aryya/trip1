package cn.wolfcode.trip.base.query;

import lombok.Getter;
import lombok.Setter;
import org.springframework.util.StringUtils;

@Getter
@Setter
public class UserQueryObject extends QueryObject {

    // 收藏查询时用到的type
    private Integer type;
    // 用户的id
    private Long userId;

    //高级查询客户的信息
    private String keyword;


    public String getKeyword() {
        return StringUtils.hasLength(keyword) ? keyword.trim() : null;
    }


}
