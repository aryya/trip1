package cn.wolfcode.trip.base.mapper;

import cn.wolfcode.trip.base.domain.Sign;
import cn.wolfcode.trip.base.query.SignQueryObject;
import java.util.Date;
import java.util.List;

public interface SignMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Sign record);

    Sign selectByPrimaryKey(Long id);

    List<Sign> selectAll();

    int updateByPrimaryKey(Sign record);

    List selectMonthSign(SignQueryObject qo);

    /**
     * 查询前一天是否有签到
     * @param date 当前时间的前一天
     * @return Sign对象
     */
    Sign getPreSignByTime(Date date);
}