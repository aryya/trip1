package cn.wolfcode.trip.base.domain;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class DailyContent extends BaseDomain{
    //日报内容
    private String content;

}