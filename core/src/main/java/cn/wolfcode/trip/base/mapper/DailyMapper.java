package cn.wolfcode.trip.base.mapper;

import cn.wolfcode.trip.base.domain.Daily;
import cn.wolfcode.trip.base.query.DailyQueryObject;
import com.github.pagehelper.PageInfo;

import java.util.List;

public interface DailyMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Daily record);

    Daily selectByPrimaryKey(Long id);

    List<Daily> selectAll();

    int updateByPrimaryKey(Daily record);

    List<Daily> selectForList(DailyQueryObject qo);

    void updateByVisitKey(Daily daily);

}