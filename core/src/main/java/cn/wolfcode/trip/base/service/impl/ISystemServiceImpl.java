package cn.wolfcode.trip.base.service.impl;

import cn.wolfcode.trip.base.domain.SystemMsg;
import cn.wolfcode.trip.base.mapper.SystemMsgMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ISystemServiceImpl implements ISystemService {
    @Autowired
    private SystemMsgMapper systemMsgMapper;


    public List<SystemMsg> getAllSytemMsgByCurrentId(Long currentId) {
        List<SystemMsg> msgList = systemMsgMapper.getAllSytemMsgByCurrentId(currentId);


        return msgList;

    }

    public void updateState(SystemMsg systemMsg) {
        systemMsgMapper.updateState(systemMsg);
    }

    public int getSystemMsgCount(Long currentId) {
        return systemMsgMapper.getSystemMsgCount(currentId);
    }
}
