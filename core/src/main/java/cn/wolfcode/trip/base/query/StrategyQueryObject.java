package cn.wolfcode.trip.base.query;

import lombok.Getter;
import lombok.Setter;
import org.springframework.util.StringUtils;

@Getter
@Setter
public class StrategyQueryObject extends QueryObject {

    //搜索关键字
    private String keyword;

    private Integer state;//攻略状态

    private Long regionId;//地区id


    public String getKeyword() {
        return StringUtils.hasLength(keyword) ? keyword.trim() : null;
    }


}
