package cn.wolfcode.trip.base.query;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ScenicSpotTicketQueryObject extends QueryObject{
    //景区id
    private Long scenicsId ;
    //地区id
    private Long toponymyId ;

}
