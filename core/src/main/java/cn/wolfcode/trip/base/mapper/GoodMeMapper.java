package cn.wolfcode.trip.base.mapper;

import cn.wolfcode.trip.base.domain.GoodMe;
import cn.wolfcode.trip.base.query.GoodMeQueryObject;
import java.util.List;

public interface GoodMeMapper {
    int deleteByPrimaryKey(Long id);

    int insert(GoodMe record);

    GoodMe selectByPrimaryKey(Long id);

    List<GoodMe> selectAll();

    int updateByPrimaryKey(GoodMe record);

    int selectGoodMeCount(Long currentId);

    /**
     * 获取当前用户的所有游记点赞信息
     * @param qo 查询条件
     * @return list集合
     */
    List getAllTravelGoode(GoodMeQueryObject qo);

    /**
     * 获取当前用户的所有评论点赞信息
     * @param qo 查询条件
     * @return list集合
     */
    List getAllCommentGoode(GoodMeQueryObject qo);

    /**
     * 更新点赞的状态
     * @param goodMe
     */
    void updateState(GoodMe goodMe);
}