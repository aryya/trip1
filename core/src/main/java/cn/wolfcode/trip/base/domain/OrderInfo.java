package cn.wolfcode.trip.base.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;

//商品订单
@Setter
@Getter
public class OrderInfo  extends BaseDomain{

    private String orderId;//订单id,用随机数生成

    private Long userId;//下单用户的id,不是根据收件人,而是当前登录的用户!

    private String userName;//下单用户的昵称

    private String userHeadImgUrl;//下单用户的头像

    private String userEmail;//下单用户的邮箱

    private Long productId;//商品id

    private String productName;//商品名

    private String productCoverUrl;//商品封面图

    private Long addrId;//收货地址的id

    private String addrUserName;//收件人姓名

    private String addrUserAddress;//收件人地址

    private String addrUserPhone;//收件人手机号

    private BigDecimal expense;//当前订单的总开销

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;//下单时间

    //0待付款, 1待收货, 2已完成
    private Integer status = 0;


}