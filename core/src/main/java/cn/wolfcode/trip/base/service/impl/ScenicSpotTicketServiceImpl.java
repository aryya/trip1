package cn.wolfcode.trip.base.service.impl;

import cn.wolfcode.trip.base.domain.ScenicSpotTicket;
import cn.wolfcode.trip.base.domain.Scenics;
import cn.wolfcode.trip.base.mapper.ScenicSpotTicketMapper;
import cn.wolfcode.trip.base.mapper.ScenicsMapper;
import cn.wolfcode.trip.base.query.DailyQueryObject;
import cn.wolfcode.trip.base.query.ScenicSpotTicketQueryObject;
import cn.wolfcode.trip.base.service.IScenicSpotTicketService;
import cn.wolfcode.trip.base.service.IScenicsService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ScenicSpotTicketServiceImpl implements IScenicSpotTicketService {

    @Autowired
    private ScenicSpotTicketMapper scenicSpotTicketMapper;


    public PageInfo query(ScenicSpotTicketQueryObject qo) {
        PageHelper.startPage(qo.getCurrentPage(),qo.getPageSize());
        List<ScenicSpotTicket> list= scenicSpotTicketMapper.selectForList(qo);
        return new PageInfo(list);

    }
}
