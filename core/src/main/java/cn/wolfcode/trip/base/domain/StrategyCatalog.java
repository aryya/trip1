package cn.wolfcode.trip.base.domain;

import cn.wolfcode.trip.base.util.JsonUtil;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * 攻略分类
 */
@Getter
@Setter
@JsonIgnoreProperties("handler")
public class StrategyCatalog extends BaseDomain {
    //名称
    private String name;
    //所属攻略
    private Strategy strategy;
    //序号
    private Integer sequence;
    //状态-- true/false , 放在前端显示
    private Boolean state;
    //catalog中查询detail的信息
    private List<StrategyDetail> strategyDetail = new ArrayList<StrategyDetail>();

    public String getJsonData() {
        HashMap<Object, Object> map = new HashMap();
        map.put("id", id);
        map.put("name", name);
        if (strategy != null) {
            map.put("strategyId", strategy.getId());
            map.put("strategyName", strategy.getTitle());
        }
        map.put("sequence", sequence);
        map.put("state", state);
        return JsonUtil.getJsonString(map);
    }
}