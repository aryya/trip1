package cn.wolfcode.trip.base.service.impl;

import cn.wolfcode.trip.base.domain.Strategy;
import cn.wolfcode.trip.base.mapper.StrategyMapper;
import cn.wolfcode.trip.base.query.QueryObject;
import cn.wolfcode.trip.base.query.StrategyQueryObject;
import cn.wolfcode.trip.base.service.IStrategyService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class StrategyServiceImpl implements IStrategyService {

    @Autowired
    private StrategyMapper strategyMapper;

    public PageInfo selectForList(StrategyQueryObject qo) {
        //设置不分页
        PageHelper.startPage(qo.getCurrentPage(), qo.getPageSize(),true,false,null);
        List list = strategyMapper.selectForList(qo);
        return new PageInfo(list);
    }

    public void saveOrUpdate(Strategy strategy) {
        if(strategy.getId()!=null){
            strategyMapper.updateByPrimaryKey(strategy);
        }else {
            strategyMapper.insert(strategy);
        }
    }

    public List selectAll() {
        return strategyMapper.selectAll();
    }

    /**
     * 一次性查询推荐的所有攻略,不需要分页
     * @param qo
     * @return
     */
    public PageInfo query(StrategyQueryObject qo) {
       PageHelper.startPage(qo.getCurrentPage(), qo.getPageSize());
        List list = strategyMapper.selectForList(qo);
        return new PageInfo(list);
    }

    public Strategy getById(Long id) {
        return strategyMapper.selectByPrimaryKey(id);
    }


}
