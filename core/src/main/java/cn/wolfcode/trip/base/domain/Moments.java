package cn.wolfcode.trip.base.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@JsonIgnoreProperties("handler")
public class Moments extends BaseDomain {
    //用户
    private User user;
    //朋友圈内容
    private String content;
    //图片
    private String image;
    //创建时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;
    //关联朋友圈评论
    private List<MomentsComment> comments=new ArrayList<MomentsComment>();

    //用于前端取值,图片显示在评论下方
    public String[] getImgArr() {
        if (StringUtils.hasLength(image)) {
            return image.split(";");
        }
        return null;
    }

}