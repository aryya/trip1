package cn.wolfcode.trip.base.query;

import lombok.Getter;
import lombok.Setter;
import org.springframework.util.StringUtils;

@Setter
@Getter
public class DailyQueryObject extends QueryObject{
    private Boolean state;//是否首推

    //搜索关键字
    private String keyword;


    public String getKeyword() {
        return StringUtils.hasLength(keyword) ? keyword.trim() : null;
    }

}
