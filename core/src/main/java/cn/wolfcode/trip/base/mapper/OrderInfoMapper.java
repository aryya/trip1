package cn.wolfcode.trip.base.mapper;

import cn.wolfcode.trip.base.domain.OrderInfo;
import cn.wolfcode.trip.base.query.OrderInfoQueryObject;

import java.util.List;

public interface OrderInfoMapper {
    int deleteByPrimaryKey(Long id);

    int insert(OrderInfo record);

    OrderInfo selectByPrimaryKey(Long id);

    List<OrderInfo> selectAll();

    int updateByPrimaryKey(OrderInfo record);

    List<OrderInfo> selectForList(OrderInfoQueryObject qo);
}