package cn.wolfcode.trip.base.query;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Getter
@Setter
public class ChatQueryObject extends QueryObject {
    private Long currentUserId; //send
    private Long chatUserId;    //receve
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date lastDate;
    private Boolean state=false;

}
