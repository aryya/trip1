package cn.wolfcode.trip.base.service.impl;

import cn.wolfcode.trip.base.domain.DailyComment;
import cn.wolfcode.trip.base.mapper.DailyCommentMapper;
import cn.wolfcode.trip.base.query.DailyCommentQueryObject;
import cn.wolfcode.trip.base.service.IDailyCommentService;
import cn.wolfcode.trip.base.util.UserContext;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class DailyCommentServiceImpl implements IDailyCommentService {
    @Autowired
    private DailyCommentMapper commentMapper;

    /**
     * 查询日版评论
     * @param qo
     * @return
     */
    public PageInfo query(DailyCommentQueryObject qo) {
        PageHelper.startPage(qo.getCurrentPage(), qo.getPageSize());
        List<DailyComment> list = commentMapper.queryForList(qo);
        return new PageInfo(list);
    }

    /**
     * 添加一条评论
     * @param comment
     */
    public void saveCommentByDaily(DailyComment comment) {
        comment.setCreatTime(new Date());
        comment.setUser(UserContext.getUser());
        if(comment.getUser()==null){
            throw new RuntimeException("没有登录");
        }
        commentMapper.insert(comment);
    }
}
