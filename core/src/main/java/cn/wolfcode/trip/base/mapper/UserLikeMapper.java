package cn.wolfcode.trip.base.mapper;

import cn.wolfcode.trip.base.domain.UserLike;
import cn.wolfcode.trip.base.query.UserQueryObject;

import java.util.List;

public interface UserLikeMapper {

    int deleteByPrimaryKey(Long id);

    int insert(UserLike record);

    UserLike selectByPrimaryKey(Long id);

    List<UserLike> selectAll();

    int updateByPrimaryKey(UserLike record);

    List<UserLike> selectStrategyDetailByUserId(UserQueryObject qo);

    List<UserLike> selectTravelByUserId(UserQueryObject qo);

    List<UserLike> selectNewPageByUserId(UserQueryObject qo);
    //收藏查询
    List selectAllS(Long userId);

    // (以下三条)搜索的查询
    List getStrategy(UserQueryObject qo);

    List getTravel(UserQueryObject qo);

    List getDaily(UserQueryObject qo);

}