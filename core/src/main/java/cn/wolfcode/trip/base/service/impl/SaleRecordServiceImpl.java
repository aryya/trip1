package cn.wolfcode.trip.base.service.impl;

import cn.wolfcode.trip.base.domain.SaleRecord;
import cn.wolfcode.trip.base.mapper.SaleRecordMapper;
import cn.wolfcode.trip.base.query.QueryObject;
import cn.wolfcode.trip.base.service.ISaleRecordService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

//消费记录服务接口
@Service
public class SaleRecordServiceImpl implements ISaleRecordService {


    @Autowired
    private SaleRecordMapper saleRecordMapper;


    public void save(SaleRecord entity) {
        saleRecordMapper.insert(entity);

    }

    public void delete(Long id) {
        saleRecordMapper.selectByPrimaryKey(id);

    }

    public void update(SaleRecord entity) {
        saleRecordMapper.updateByPrimaryKey(entity);
    }

    public SaleRecord get(Long id) {

        return saleRecordMapper.selectByPrimaryKey(id);
    }

    public List<SaleRecord> listAll() {

        return saleRecordMapper.selectAll();
    }

    public PageInfo<SaleRecord> query(QueryObject qo) {
        PageHelper.startPage(qo.getCurrentPage(), qo.getPageSize(), "sale_time desc");
        List<SaleRecord> list = saleRecordMapper.selectForList(qo);
        return new PageInfo<SaleRecord>(list);
    }
}
