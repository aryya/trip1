package cn.wolfcode.trip.base.domain;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Follow {

    //关注人的id
    private Long followId;
    //粉丝的id
    private Long followerId;


}