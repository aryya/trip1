package cn.wolfcode.trip.base.service.impl;

import cn.wolfcode.trip.base.domain.StrategyCatalog;
import cn.wolfcode.trip.base.mapper.StrategyCatalogMapper;
import cn.wolfcode.trip.base.query.QueryObject;
import cn.wolfcode.trip.base.service.IStrategyCatalogService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StrategyCatalogServiceImpl implements IStrategyCatalogService {

    @Autowired
    private StrategyCatalogMapper catalogMapper;

    public PageInfo selectForList(QueryObject qo) {
        PageHelper.startPage(qo.getCurrentPage(), qo.getPageSize());
        List list = catalogMapper.selectForList(qo);
        return new PageInfo(list);
    }

    public void saveOrUpdate(StrategyCatalog strategyCatalog) {
        if (strategyCatalog.getSequence() == null) {
            //查询每个攻略下最大的排序
            Integer maxSequence = catalogMapper.getMaxSequence(strategyCatalog.getStrategy().getId());
            //如果用户不设置,就自增
            strategyCatalog.setSequence(maxSequence + 1);
        }
        if (strategyCatalog.getId() != null) {
            catalogMapper.updateByPrimaryKey(strategyCatalog);
        } else {
            catalogMapper.insert(strategyCatalog);
        }
    }

    public List<StrategyCatalog> listByStrategyId(Long strategyId) {
        return catalogMapper.listByStrategyId(strategyId);
    }


}
