package cn.wolfcode.trip.base.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class TravelComment extends BaseDomain{
    // 用户
    private User user;
    // 内容
    private String content;
    // 评论时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm",timezone = "GMT+8")
    private Date carentTime;
    // 游记
    private Travel travel;
}