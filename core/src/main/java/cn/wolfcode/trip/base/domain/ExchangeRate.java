package cn.wolfcode.trip.base.domain;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Setter
@Getter
public class ExchangeRate extends BaseDomain{
    //名称
    private String name;
    //缩写
    private String minName;
    //内部价
    private BigDecimal insidePrice;
    //市场价
    private BigDecimal marketPrice;
    //图片
    private String imgUrl;

}