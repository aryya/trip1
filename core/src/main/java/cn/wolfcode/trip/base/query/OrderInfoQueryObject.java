package cn.wolfcode.trip.base.query;


import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class OrderInfoQueryObject extends QueryObject {
    //用于查询对应用户的对应订单
    private Long userId;

    //用于查询指定状态的订单
    private Integer status;

}
