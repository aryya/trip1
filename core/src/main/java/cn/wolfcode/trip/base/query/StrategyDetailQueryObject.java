package cn.wolfcode.trip.base.query;

import lombok.Getter;
import lombok.Setter;
import org.springframework.util.StringUtils;

@Getter
@Setter
public class StrategyDetailQueryObject extends QueryObject {

    //搜索关键字
    private String keyword;


    public String getKeyword() {
        return StringUtils.hasLength(keyword) ? keyword.trim() : null;
    }


}
