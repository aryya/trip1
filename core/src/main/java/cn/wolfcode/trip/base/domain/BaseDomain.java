package cn.wolfcode.trip.base.domain;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public abstract class BaseDomain {

    protected Long id;
}
