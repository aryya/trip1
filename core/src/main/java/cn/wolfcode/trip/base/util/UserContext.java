package cn.wolfcode.trip.base.util;
import cn.wolfcode.trip.base.domain.User;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import javax.servlet.http.HttpSession;

public class UserContext {

    public static final String USER_IN_SESSION = "USER_IN_SESSION";

    /**
     * 获取session对象
     * @return
     */
    public static HttpSession getSession() {
        return ((ServletRequestAttributes) (RequestContextHolder.getRequestAttributes()))
                .getRequest().getSession();
    }

    /**
     * 把登录的user对象信息保存在session中
     * @param user
     */
    public static void setUser(User user) {
        if (user == null) {
            getSession().invalidate();
        } else {
            getSession().setAttribute(USER_IN_SESSION, user);
        }
    }

    /**
     * 从session中获取user对象
     * @return
     */
    public static User getUser() {
        return (User) getSession().getAttribute(USER_IN_SESSION);
    }
}
