package cn.wolfcode.trip.base.service.impl;

import cn.wolfcode.trip.base.mapper.UserLikeMapper;
import cn.wolfcode.trip.base.query.UserQueryObject;
import cn.wolfcode.trip.base.service.IUserLikeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class UserLikeSerivceImpl implements IUserLikeService {


    @Autowired
    private UserLikeMapper userLikeMapper;

    public List selectByUserId(UserQueryObject qo) {
        if(qo.getType()==1){

            List list = userLikeMapper.selectStrategyDetailByUserId(qo);
            return  list;
        }else
        if(qo.getType()==2){

            List list = userLikeMapper.selectTravelByUserId(qo);
            return  list;
        }else
        if(qo.getType()==3){

            List list = userLikeMapper.selectNewPageByUserId(qo);
            return  list;
        }

        return  null;
    }

    public List<Map<String ,Object>> selectAll(Long userId) {

        return userLikeMapper.selectAllS(userId);
    }


    // 搜索攻略
    public List getStrategys(UserQueryObject qo) {

        return userLikeMapper.getStrategy(qo);
    }

    // 搜索游记
    public List getTravel(UserQueryObject qo) {

        return userLikeMapper.getTravel(qo);
    }

    // 搜索日报
    public List getDaily(UserQueryObject qo) {

        return userLikeMapper.getDaily(qo);
    }
}
