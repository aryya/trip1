package cn.wolfcode.trip.base.service.impl;

import cn.wolfcode.trip.base.domain.Region;
import cn.wolfcode.trip.base.mapper.RegionMapper;
import cn.wolfcode.trip.base.query.StrategyQueryObject;
import cn.wolfcode.trip.base.service.IRegionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class RegionServiceImpl implements IRegionService {

    @Autowired
    private RegionMapper regionMapper;

    public int deleteByPrimaryKey(Long id) {
        return regionMapper.deleteByPrimaryKey(id);
    }

    public int insert(Region record) {
        return regionMapper.insert(record);
    }

    public Region selectByPrimaryKey(Long id) {
        return regionMapper.selectByPrimaryKey(id);
    }

    public List<Region> selectAll(Integer state) {
        return regionMapper.selectAll(state);
    }

    public int updateByPrimaryKey(Region record) {
        return regionMapper.updateByPrimaryKey(record);
    }

    public List<Region> selectByParentId(Long parentId) {
        return regionMapper.listByParentId(parentId);
    }

    public void updateStatus(Long id, Integer state) {

        regionMapper.updateStatus(id, state);
    }

    public List<Region> listAll(StrategyQueryObject qo) {
        return regionMapper.selectAll(null);
    }
}
