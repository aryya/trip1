package cn.wolfcode.trip.base.service;

import cn.wolfcode.trip.base.domain.Daily;
import cn.wolfcode.trip.base.domain.LocationSituation;
import cn.wolfcode.trip.base.mapper.LocationSituationMapper;

public interface ILocationSituationService {

    LocationSituation get(Long id);
}
