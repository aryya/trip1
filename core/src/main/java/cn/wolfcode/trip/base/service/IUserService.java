package cn.wolfcode.trip.base.service;

import cn.wolfcode.trip.base.domain.Follow;
import cn.wolfcode.trip.base.domain.StrategyComment;
import cn.wolfcode.trip.base.domain.User;
import cn.wolfcode.trip.base.query.QueryObject;
import cn.wolfcode.trip.base.query.UserCommentsQueryObject;
import com.github.pagehelper.PageInfo;import java.util.List;
import cn.wolfcode.trip.base.query.UserQueryObject;
import com.github.pagehelper.PageInfo;

import java.util.List;

public interface IUserService {

    int insert(User record);

    User selectByPrimaryKey(Long id);

    List<User> selectAll();

    int updateByPrimaryKey(User record);

    /**
     * 注册用户
     * @param user
     */
    void register(User user);

    /**
     * 用户登录
     * @param email
     * @param password
     * @return
     */
    User login(String email,String password);

    PageInfo<User> queryUserInfo(QueryObject qo);

    /**
     * 查询相互关注的id
     * @param userId
     * @param authorId
     */
    Follow listMutualRelationship(Long userId, Long authorId);

    /**
     * 新增相互关注的id
     * @param userId
     * @param authorId
     */
    void saveMutualRelationship(Long userId, Long authorId);

    int deleteMutualRelationship(Long userId, Long authorId);

    int listFollowNum(Long userId);

    int listFollowerNum(Long authorId);

    PageInfo queryCommetsByUserId(QueryObject qo);

    PageInfo listFollowsById(UserQueryObject qo);
    /**
     * 查询用户的评论
     * @param qo
     * @return
     */
    List listCommentsByUserId(UserCommentsQueryObject qo);

    /**
     * 查询用户未读的评论数
     * @param userId
     * @return
     */
    int getCommentTips(Long userId);

    /**
     * 删除主页评论的红点数
     * @param userId
     */
    void deleteCommentNum(Long userId);

    /**
     * 查询头像
     * @param userId
     * @return
     */
    String getUserHeadImgUrl(Long userId);
}
