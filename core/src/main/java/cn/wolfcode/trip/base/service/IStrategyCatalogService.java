package cn.wolfcode.trip.base.service;


import cn.wolfcode.trip.base.domain.StrategyCatalog;
import cn.wolfcode.trip.base.query.QueryObject;
import com.github.pagehelper.PageInfo;

import java.util.List;

public interface IStrategyCatalogService {

    PageInfo selectForList(QueryObject qo);

    void saveOrUpdate(StrategyCatalog strategyCatalog);

    List<StrategyCatalog> listByStrategyId(Long stratageId);
}
