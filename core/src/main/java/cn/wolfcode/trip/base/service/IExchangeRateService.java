package cn.wolfcode.trip.base.service;

import cn.wolfcode.trip.base.query.QueryObject;
import com.github.pagehelper.PageInfo;

import java.math.BigDecimal;

public interface IExchangeRateService {
    // 查询所有的货币
    PageInfo listAll(QueryObject qo);
    // 查询汇率
    BigDecimal selectInSidePriceById(Long id);
    // 计算汇率
    BigDecimal countCountRates(Long exchangeRateId, BigDecimal money);
    // 查询缩写
    String selectRatesMinName(Long exchangeRateId);
}
