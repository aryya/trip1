package cn.wolfcode.trip.base.service;


import cn.wolfcode.trip.base.domain.TravelCommend;
import cn.wolfcode.trip.base.query.TravelCommendQueryObject;
import com.github.pagehelper.PageInfo;

public interface ITravelCommendService {

    /**
     * 分页
     * @param qo
     * @return
     */
    PageInfo selectForList(TravelCommendQueryObject qo);

    /**
     * 保存和更新
     * @param travelCommend
     */
    void saveOrUpdate(TravelCommend travelCommend);

    /**
     * 分页
     * @param qo
     * @return
     */
    PageInfo queryForApp(TravelCommendQueryObject qo);
}
