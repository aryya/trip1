package cn.wolfcode.trip.base.service.impl;

import cn.wolfcode.trip.base.domain.Follow;
import cn.wolfcode.trip.base.domain.StrategyComment;
import cn.wolfcode.trip.base.domain.User;
import cn.wolfcode.trip.base.mapper.StrategyCommentMapper;
import cn.wolfcode.trip.base.mapper.TravelCommentMapper;
import cn.wolfcode.trip.base.mapper.FollowMapper;
import cn.wolfcode.trip.base.mapper.StrategyCommentMapper;
import cn.wolfcode.trip.base.mapper.UserMapper;
import cn.wolfcode.trip.base.query.QueryObject;
import cn.wolfcode.trip.base.query.UserCommentsQueryObject;
import cn.wolfcode.trip.base.query.UserQueryObject;
import cn.wolfcode.trip.base.service.IUserService;
import cn.wolfcode.trip.base.util.UserContext;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserServiceImpl implements IUserService {

    @Autowired
    private UserMapper userMapper;
    @Autowired
    private StrategyCommentMapper commentMapper;
    @Autowired
    private TravelCommentMapper travelCommentMapper;
    @Autowired
    private FollowMapper followMapper;

    public int insert(User record) {
        return userMapper.insert(record);
    }

    public User selectByPrimaryKey(Long id) {
        return userMapper.selectByPrimaryKey(id);
    }

    public List<User> selectAll() {
        return userMapper.selectAll();
    }

    public int updateByPrimaryKey(User record) {
        return userMapper.updateByPrimaryKey(record);
    }

    public void register(User user) {

        User registerEmail = userMapper.selectByEmailOrPassword(user.getEmail(), null);
        if (registerEmail != null) {
            //如果不为空,提示用户该邮箱已注册
            throw new RuntimeException("该邮箱已注册");
        }
        user.setHeadImgUrl("/img/user/head.jpg");
        user.setCoverImgUrl("/img/user/bg.jpeg");
        user.setGender(1);
        //保存数据
        userMapper.insert(user);
    }

    /**
     * 登录账号
     *
     * @param email
     * @param password
     * @return
     */
    public User login(String email, String password) {

        User user = userMapper.selectByEmailOrPassword(email, password);
        if (user == null) {
            throw new RuntimeException("亲,您的账号和密码不匹配");
        }
        UserContext.setUser(user);
        return user;
    }

    //分页
    public PageInfo<User> queryUserInfo(QueryObject qo) {
        PageHelper.startPage(qo.getCurrentPage(), qo.getPageSize());
        List<User> users = userMapper.selectForList(qo);
        return new PageInfo<User>(users);
    }

    /**
     * 用户的评论
     * @param qo
     * @return
     */
    public List listCommentsByUserId(UserCommentsQueryObject qo) {
        List list = new ArrayList();
        if(qo.getState()==1){
            list = commentMapper.queryCommentsByUserId(qo);
        }else if(qo.getState()==2){
            list = travelCommentMapper.queryCommentsByUserId(qo);
        }else {

        }
        return list;
    }

    // 查询用户的未读评论数
    public int getCommentTips(Long userId) {
        return userMapper.selectCommentTipsByUserId(userId);
    }
    // 删除评论的红点数
    public void deleteCommentNum(Long userId) {
        userMapper.deleteCommentNum(userId);
    }
    // 查询用户头像
    public String getUserHeadImgUrl(Long userId) {
        return userMapper.selectHeadUrl(userId);
    }

    //查询关注列表是否存在相互关系
    public Follow listMutualRelationship(Long userId, Long authorId) {
        return followMapper.selectMutualRelationship(userId, authorId);
    }

    public void saveMutualRelationship(Long userId, Long authorId) {
        followMapper.insertMutualRelationship(userId, authorId);
    }

    public int deleteMutualRelationship(Long userId, Long authorId) {
        return followMapper.deleteMutualRelationship(userId, authorId);
    }

    public int listFollowNum(Long userId) {
        return followMapper.selectFollowNum(userId);
    }

    public int listFollowerNum(Long authorId) {
        int i = followMapper.selectFollowerNum(authorId);
        return i;
    }

    /**
     * 查询个人页面的所有评论
     *
     * @param qo
     * @return
     */
    public PageInfo queryCommetsByUserId(QueryObject qo) {
        PageHelper.startPage(qo.getCurrentPage(), qo.getPageSize(), qo.getOrderBy());
        List list = commentMapper.selectCommetsByUserId(qo);
        return new PageInfo(list);
    }

    /**
     * 查询个人页面的所有关注人
     *
     * @param qo
     * @return
     */
    public PageInfo listFollowsById(UserQueryObject qo) {
        PageHelper.startPage(qo.getCurrentPage(), qo.getPageSize());
        List list = userMapper.selectFollowsById(qo);
        return new PageInfo(list);
    }

}
