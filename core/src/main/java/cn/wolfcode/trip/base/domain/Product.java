package cn.wolfcode.trip.base.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 积分商城
 */
@Setter
@Getter
public class Product extends BaseDomain {

    private String name; // 商品名

    private BigDecimal originalPrice; //商品原价

    private BigDecimal price; //商品抢购价

    private String type; //类型

    private String coverUrl; //商品封面

    private Integer salenum; //商品销量

    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date createTime; //商品创建时间

}