package cn.wolfcode.trip.base.service;

import cn.wolfcode.trip.base.domain.ScenicSpotTicket;
import cn.wolfcode.trip.base.query.DailyQueryObject;
import cn.wolfcode.trip.base.query.ScenicSpotTicketQueryObject;
import com.github.pagehelper.PageInfo;

import java.util.List;

public interface IScenicSpotTicketService {
    /**
     * 景区分页
     * @param qo
     * @return
     */
    PageInfo query(ScenicSpotTicketQueryObject qo);


}
