package cn.wolfcode.trip.base.mapper;

import cn.wolfcode.trip.base.domain.CommentReply;
import cn.wolfcode.trip.base.query.QueryObject;
import cn.wolfcode.trip.base.query.ReplyQueryObject;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CommentReplyMapper {
    int deleteByPrimaryKey(Long id);

    int insert(CommentReply record);

    CommentReply selectByPrimaryKey(Long id);

    List<CommentReply> selectAll();

    int updateByPrimaryKey(CommentReply record);

    // 查询所有一级评论
    List<CommentReply> selectReplyByCommentId(ReplyQueryObject qo);
    // 查询所有二级评论
    List<CommentReply> listByParentId(@Param("parentId") Long parentId);
    // 统计评论数
    int selectCountReply(Long commentId);
    // 查询所有的未读评论数
    List<CommentReply> listStrategyCommentByUserId(Long userId);

    void insertCommentIdAndUserId(@Param("id") Long id, @Param("userId") Long userId);
}