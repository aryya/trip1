package cn.wolfcode.trip.base.domain;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Scenics extends BaseDomain{

    //上级id
    private Long parent_id;

    //景点名称
    private String name;


}