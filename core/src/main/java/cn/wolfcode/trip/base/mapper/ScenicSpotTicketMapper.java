package cn.wolfcode.trip.base.mapper;

import cn.wolfcode.trip.base.domain.ScenicSpotTicket;
import cn.wolfcode.trip.base.domain.Scenics;
import cn.wolfcode.trip.base.query.ScenicSpotTicketQueryObject;
import java.util.List;

public interface ScenicSpotTicketMapper {
    int deleteByPrimaryKey(Long id);

    int insert(ScenicSpotTicket record);

    ScenicSpotTicket selectByPrimaryKey(Long id);

    List<ScenicSpotTicket> selectAll();

    int updateByPrimaryKey(ScenicSpotTicket record);

    List<ScenicSpotTicket> selectForList(ScenicSpotTicketQueryObject qo);
}