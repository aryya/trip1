package cn.wolfcode.trip.base.service;

import cn.wolfcode.trip.base.domain.UserAddress;

import java.util.List;


//收货地址服务接口
public interface IUserAddressService {
    /**
     * 新增收货地址
     *
     * @param entity
     */
    void save(UserAddress entity);

    /**
     * 收货地址删除
     * @param id
     */
    void delete(Long addressId);

    /**
     * 编辑收货地址
     * @param entity
     */
    void update(UserAddress entity);

    /**
     * 查询收货地址
     * @param id
     * @return
     */
    UserAddress get(Long id);

    /**
     *查询
     * @param userId
     * @return
     */
    List<UserAddress> listAll(Long userId);

    /**
     * 编辑用户默认地址
     * @param userId 用户id
     * @param addressId 收货地址id
     */
    void updateDefaultAddress(Long userId, Long addressId);

    //List<UserAddress> listAll()
}
