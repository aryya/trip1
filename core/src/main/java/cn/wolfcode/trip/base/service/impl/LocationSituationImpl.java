package cn.wolfcode.trip.base.service.impl;

import cn.wolfcode.trip.base.domain.LocationSituation;
import cn.wolfcode.trip.base.mapper.LocationSituationMapper;
import cn.wolfcode.trip.base.service.ILocationSituationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LocationSituationImpl implements ILocationSituationService {

    @Autowired
    private LocationSituationMapper locationSituationMapper;


    public LocationSituation get(Long id) {
        return locationSituationMapper.selectByPrimaryKey(id);
    }
}
