package cn.wolfcode.trip.base.service.impl;

import cn.wolfcode.trip.base.domain.Product;
import cn.wolfcode.trip.base.mapper.ProductMapper;
import cn.wolfcode.trip.base.query.ProductQueryObject;
import cn.wolfcode.trip.base.service.IProductService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class ProductServiceImpl implements IProductService {

    @Autowired
    private ProductMapper productMapper;


    public void save(Product entity) {
        productMapper.insert(entity);

    }

    public void delete(Long id) {
        productMapper.deleteByPrimaryKey(id);
    }

    public void update(Product entity) {
        productMapper.updateByPrimaryKey(entity);

    }

    public Product get(Long id) {

        return productMapper.selectByPrimaryKey(id);
    }

    public List<Product> listAll() {

        return productMapper.selectAll();
    }

    //商品列表分页
    public PageInfo<Product> query(ProductQueryObject qo) {
        PageHelper.startPage(qo.getCurrentPage(), qo.getPageSize(), qo.getOrderBy());
        List<Product> list = productMapper.selectForList(qo);
        return new PageInfo<Product>(list);
    }
}
