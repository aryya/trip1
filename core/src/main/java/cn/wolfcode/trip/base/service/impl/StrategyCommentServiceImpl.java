package cn.wolfcode.trip.base.service.impl;

import cn.wolfcode.trip.base.domain.StrategyComment;
import cn.wolfcode.trip.base.domain.Tag;
import cn.wolfcode.trip.base.mapper.StrategyCommentMapper;
import cn.wolfcode.trip.base.mapper.TagMapper;
import cn.wolfcode.trip.base.query.QueryObject;
import cn.wolfcode.trip.base.query.StrategyCommentQueryObject;
import cn.wolfcode.trip.base.service.IStrategyCommentService;
import cn.wolfcode.trip.base.util.UserContext;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class StrategyCommentServiceImpl implements IStrategyCommentService {

    @Autowired
    private StrategyCommentMapper commentMapper;
    @Autowired
    private TagMapper tagMapper;

    /**
     * admin后台显示所有的评论信息
     *
     * @param qo
     * @return
     */
    public PageInfo selectForList(StrategyCommentQueryObject qo) {
        PageHelper.startPage(qo.getCurrentPage(), qo.getPageSize());
        List list = commentMapper.selectForList(qo);
        return new PageInfo(list);
    }

    public void updateStausById(StrategyComment strategyComment) {
        commentMapper.updateStausById(strategyComment);
    }

    /**
     * app页面根据攻略id显示所有的评论信息
     *
     * @param qo
     * @return
     */
    public PageInfo listCommentByStrategyId(StrategyCommentQueryObject qo) {
        PageHelper.startPage(qo.getCurrentPage(), qo.getPageSize(), qo.getOrderBy());
        List list = commentMapper.selectForList(qo);
        return new PageInfo(list);
    }

    /**
     * 新增点评信息
     *
     * @param strategyComment
     */
    public void save(StrategyComment strategyComment, String[] tags) {
        //设置用户
        strategyComment.setUser(UserContext.getUser());
        //设置新建评论时间
        strategyComment.setCreateTime(new Date());
        commentMapper.insert(strategyComment);
        for (String tagStr : tags) {
            Tag tag = new Tag();
            tag.setName(tagStr);
            tagMapper.insert(tag);

            commentMapper.insertCommentTagRelation(strategyComment.getId(), tag.getId());
        }

    }

    /**
     * 查询标签出来
     * @param qo
     * @return
     */
    public PageInfo listTagsByStrategyId(StrategyCommentQueryObject qo) {
        PageHelper.startPage(qo.getCurrentPage(), qo.getPageSize());
        List list = tagMapper.selectTagsByStrategyId(qo);
        return new PageInfo(list);
    }

    /**
     * 根据攻略评论id查评论信息
     * @param id
     * @return
     */
    public StrategyComment getCommentByCommentId(Long id) {
        return commentMapper.selectByPrimaryKey(id);
    }


}
