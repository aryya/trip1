package cn.wolfcode.trip.base.mapper;

import cn.wolfcode.trip.base.domain.Scenics;
import cn.wolfcode.trip.base.query.DailyQueryObject;
import cn.wolfcode.trip.base.query.QueryObject;

import java.util.List;

public interface ScenicsMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Scenics record);

    Scenics selectByPrimaryKey(Long id);

    List<Scenics> selectAll();

    int updateByPrimaryKey(Scenics record);

    List<Scenics> selectForList(QueryObject qo);
}