package cn.wolfcode.trip.base.service;

import cn.wolfcode.trip.base.domain.StrategyComment;
import cn.wolfcode.trip.base.query.StrategyCommentQueryObject;
import com.github.pagehelper.PageInfo;

import java.util.List;


public interface IStrategyCommentService {

    PageInfo selectForList(StrategyCommentQueryObject qo);

    void updateStausById(StrategyComment strategyComment);

    PageInfo listCommentByStrategyId(StrategyCommentQueryObject qo);

    void save(StrategyComment strategyComment, String[] tags);

    PageInfo listTagsByStrategyId(StrategyCommentQueryObject qo);

    StrategyComment getCommentByCommentId(Long id);
}
