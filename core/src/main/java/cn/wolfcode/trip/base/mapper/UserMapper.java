package cn.wolfcode.trip.base.mapper;

import cn.wolfcode.trip.base.domain.StrategyComment;
import cn.wolfcode.trip.base.domain.User;
import cn.wolfcode.trip.base.query.ChatQueryObject;
import cn.wolfcode.trip.base.query.QueryObject;
import cn.wolfcode.trip.base.query.UserQueryObject;
import org.apache.ibatis.annotations.Param;
import java.util.List;
import java.util.Set;

public interface UserMapper {

    void deleteByPrimaryKey(Long id);

    int insert(User record);

    User selectByPrimaryKey(Long id);

    List<User> selectAll();

    int updateByPrimaryKey(User record);

    User selectByEmailOrPassword(@Param("email") String email, @Param("psd") String password);

    //分页
    List<User> selectForList(QueryObject qo);


    List selectFollowsById(UserQueryObject qo);

    /**
     * 根据当前用户查询私信对象
     * @param qo
     * @return
     */
    List selectListReceivers(ChatQueryObject qo);

    // 查询用户的未读评论数
    int selectCommentTipsByUserId(Long userId);
    // 删除红点数
    void deleteCommentNum(Long userId);
    // 查询用户头像
    String selectHeadUrl(Long userId);
}